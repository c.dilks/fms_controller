#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ////////////////////////////////////////////////////////////////////////////////////

  // SIMULATION MODE SETTING: if true, all status lights and switches are "simulated", that is
  // no actual HV or UV setting changes or readings are executed; this is for the purpose of testing
  // the front end and interlock behavior
  simulation_mode = 0;

  // DEBUGGING PRINTING MODE: if true, prints extra kDebug log messages
  debug = 0;

  // DEBUGGING MODE for backend scripts
  // 0 turns off all debugging
  // 1 turns on more verbose log messages
  // 2 (eventually) turns on extra terminal windows (or something...)
  debug_backend = 0;

  // bank current load "alarm threshold" (for sending critical messages)
  alarm_current = 18.5;

  // threshold voltage: this is the voltage which a lecroy crate average voltage must be below
  // in order to be considered "off"
  voltage_threshold = 100.0;

  // lecroy voltage ramp time: number of seconds to wait after applying change to lecroy crates before
  // checking if the voltages have finished ramping
  lecroy_ramp_time = 30;


  ////////////////////////////////////////////////////////////////////////////////////

  // set up GUI
  ui->setupUi(this);

  // assign images to pixmaps
  hv_light_image[k7005][kOn] = new QPixmap(":/status_lights/img/7005_on.png");
  hv_light_image[k7005][kOff] = new QPixmap(":/status_lights/img/7005_off.png");
  hv_light_image[k7005][kUpdating] = new QPixmap(":/status_lights/img/7005_updating.png");
  hv_light_image[k7005][kUnknown] = new QPixmap(":/status_lights/img/7005_unknown.png");
  hv_light_image[k7005][kRamping] = new QPixmap(":/status_lights/img/7005_ramping.png");

  hv_light_image[k7006][kOn] = new QPixmap(":/status_lights/img/7006_on.png");
  hv_light_image[k7006][kOff] = new QPixmap(":/status_lights/img/7006_off.png");
  hv_light_image[k7006][kUpdating] = new QPixmap(":/status_lights/img/7006_updating.png");
  hv_light_image[k7006][kUnknown] = new QPixmap(":/status_lights/img/7006_unknown.png");
  hv_light_image[k7006][kRamping] = new QPixmap(":/status_lights/img/7006_ramping.png");

  hv_light_image[k7007][kOn] = new QPixmap(":/status_lights/img/7007_on.png");
  hv_light_image[k7007][kOff] = new QPixmap(":/status_lights/img/7007_off.png");
  hv_light_image[k7007][kUpdating] = new QPixmap(":/status_lights/img/7007_updating.png");
  hv_light_image[k7007][kUnknown] = new QPixmap(":/status_lights/img/7007_unknown.png");
  hv_light_image[k7007][kRamping] = new QPixmap(":/status_lights/img/7007_ramping.png");

  hv_light_image[k7008][kOn] = new QPixmap(":/status_lights/img/7008_on.png");
  hv_light_image[k7008][kOff] = new QPixmap(":/status_lights/img/7008_off.png");
  hv_light_image[k7008][kUpdating] = new QPixmap(":/status_lights/img/7008_updating.png");
  hv_light_image[k7008][kUnknown] = new QPixmap(":/status_lights/img/7008_unknown.png");
  hv_light_image[k7008][kRamping] = new QPixmap(":/status_lights/img/7008_ramping.png");

  hv_light_image[k6V][kOn] = new QPixmap(":/status_lights/img/6V_on.png");
  hv_light_image[k6V][kOff] = new QPixmap(":/status_lights/img/6V_off.png");
  hv_light_image[k6V][kUpdating] = new QPixmap(":/status_lights/img/6V_updating.png");
  hv_light_image[k6V][kUnknown] = new QPixmap(":/status_lights/img/6V_unknown.png");

  hv_light_image[k9V][kOn] = new QPixmap(":/status_lights/img/9V_on.png");
  hv_light_image[k9V][kOff] = new QPixmap(":/status_lights/img/9V_off.png");
  hv_light_image[k9V][kUpdating] = new QPixmap(":/status_lights/img/9V_updating.png");
  hv_light_image[k9V][kUnknown] = new QPixmap(":/status_lights/img/9V_unknown.png");

  hv_light_image[k30V][kOn] = new QPixmap(":/status_lights/img/30V_on_long.png");
  hv_light_image[k30V][kOff] = new QPixmap(":/status_lights/img/30V_off_long.png");
  hv_light_image[k30V][kUpdating] = new QPixmap(":/status_lights/img/30V_updating_long.png");
  hv_light_image[k30V][kUnknown] = new QPixmap(":/status_lights/img/30V_unknown_long.png");

  uv_light_image[kOn] = new QPixmap(":/status_lights/img/uv_light_on.png");
  uv_light_image[kOff] = new QPixmap(":/status_lights/img/uv_light_off.png");
  uv_light_image[kUpdating] = new QPixmap(":/status_lights/img/uv_light_updating.png");
  uv_light_image[kUnknown] = new QPixmap(":/status_lights/img/uv_light_unknown.png");

  lock_icon[kLocked] = new QPixmap(":/status_lights/img/interlock_locked.png");
  lock_icon[kUnlocked] = new QPixmap(":/status_lights/img/interlock_unlocked.png");


  // misc initializations
  sprintf(sty_black,"background-color: black; color: black");
  currently_changing_hv[kLarge] = false;
  currently_changing_hv[kSmall] = false;
  currently_changing_uv[kNorth] = false;
  currently_changing_uv[kSouth] = false;

  sprintf(hv_name[k7005],"Lecroy crate 7005");
  sprintf(hv_name[k7006],"Lecroy crate 7006");
  sprintf(hv_name[k7007],"Lecroy crate 7007");
  sprintf(hv_name[k7008],"Lecroy crate 7008");
  sprintf(hv_name[k30V],"30V high voltage");
  sprintf(hv_name[k9V],"9V low voltage");
  sprintf(hv_name[k6V],"6V low voltage");

  sprintf(hv_name_short[k7005],"7005"); // DO NOT CHANGE! these are
  sprintf(hv_name_short[k7006],"7006"); // used as arguments for back-end scripts
  sprintf(hv_name_short[k7007],"7007");
  sprintf(hv_name_short[k7008],"7008");
  sprintf(hv_name_short[k30V],"30V");
  sprintf(hv_name_short[k9V],"9V");
  sprintf(hv_name_short[k6V],"6V");

  sprintf(pdu_name[kNorth],"North");
  sprintf(pdu_name[kSouth],"South");

  sprintf(pdu_name_short[kNorth],"north"); // DO NOT CHANGE these are 
  sprintf(pdu_name_short[kSouth],"south"); // used as arguments for back-end scripts

  sprintf(state_name[kOn],"ON");
  sprintf(state_name[kOff],"OFF");
  sprintf(state_name[kUnknown],"UNKNOWN");
  sprintf(state_name[kRamping],"RAMPING");

  sprintf(state_name_short[kOn],"on");    // DO NOT CHANGE! these are
  sprintf(state_name_short[kOff],"off");  // used as arguments for back-end scripts
  sprintf(state_name_short[kUnknown],"unknown"); 
  sprintf(state_name_short[kRamping],"ramping"); 

  sprintf(interlock_status_name[kLocked],"LOCKED");
  sprintf(interlock_status_name[kUnlocked],"UNLOCKED");

  sprintf(bank_name[kB1],"Bank 1");
  sprintf(bank_name[kB2],"Bank 2");
  sprintf(bank_name[kB3],"Bank 3");

  message_type_str[kNote] = QString("<font color=\"Aqua\">NOTE");
  message_type_str[kError] = QString("<font color=\"Red\">ERROR");
  message_type_str[kAction] = QString("<font color=\"Yellow\">ACTION");
  message_type_str[kCritical] = QString("<font color=\"Magenta\">CRITICAL");
  message_type_str[kBackend] = QString("<font color=\"Tan\">BACKEND");
  message_type_str[kDebug] = QString("<font color=\"Silver\">DEBUG");
  message_type_str[kUpdateMessage] = QString("<font color=\"White\">UPDATE");
  message_type_str[kSMSonly] = QString("");

  // bank current maps
  for(int ns=0; ns<2; ns++) {
    bank_map[ns][kTop][kps1] = kB1;
    bank_map[ns][kTop][kps2] = kB1;
    bank_map[ns][kTop][kps3] = kB2;
    bank_map[ns][kTop][kps4] = kB2;
    bank_map[ns][kTop][kps5] = kB3;
    bank_map[ns][kTop][kps6] = kB3;
  
    bank_map[ns][kBottom][kps1] = kB1;
    bank_map[ns][kBottom][kps2] = kB1;
    bank_map[ns][kBottom][kps3] = kB2;
    bank_map[ns][kBottom][kps4] = kB2;
    bank_map[ns][kBottom][kps5] = kB3;
    bank_map[ns][kBottom][kps6] = kB3;
  };


  // major_problem boolean -- if true, system is in "panic mode" and experts need to be called;
  // only restarting the GUI will clear the "panic"
  major_problem = false;


  // update only hook; defaults to uAll, but if it's set to anything else, when setUpdate_uv_status
  // is called, it only updates specific crate status or fpd switch only
  hv_update_only = uAll;
  uv_update_only = uAll;


  // initialize timers
  hv_update_timer = new QTimer(this);
  uv_update_timer = new QTimer(this);
  uv_change_timer[kNorth] = new QTimer(this);
  uv_change_timer[kSouth] = new QTimer(this);
  censor_timer = new QTimer(this);



  // configure backend directories for properly executing scripts
  sprintf(scriptdir,"%s","../src/backend");
  sprintf(logdir,"%s","../src/log");
  sprintf(pushd,"pushd %s > /dev/null",scriptdir);
  sprintf(pushdlog,"pushd %s > /dev/null",logdir);
  sprintf(popd,"%s","popd > /dev/null");
  if(debug_backend>0) {
    print_log(kBackend,QString("scriptdir -=- %1").arg(scriptdir));
    print_log(kBackend,QString("pushd -=- %1").arg(pushd));
    print_log(kBackend,QString("popd -=- %1").arg(popd));
  };

  

  // initialize all statuses (but don't yet update lights or switches)
  initialize_statuses();



  // CONNECTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // HV shift control buttons
  connect(ui->large_hv_on,  SIGNAL (clicked()), this, SLOT (setLarge_on()));
  connect(ui->small_hv_on,  SIGNAL (clicked()), this, SLOT (setSmall_on()));

  connect(ui->large_hv_off, SIGNAL (clicked()), this,  SLOT (setLarge_off()));
  connect(ui->small_hv_off, SIGNAL (clicked()), this,  SLOT (setSmall_off()));

  connect(ui->update_hv, SIGNAL (clicked()), this,  SLOT (setUpdate_hv_status()));

  // exit button
  connect(ui->exit_button, SIGNAL (clicked()), this, SLOT (close()));

  // lecroy control buttons
  connect(ui->lecroy_7005_on, SIGNAL (clicked()), this,  SLOT (set7005_on()));
  connect(ui->lecroy_7006_on, SIGNAL (clicked()), this,  SLOT (set7006_on()));
  connect(ui->lecroy_7007_on, SIGNAL (clicked()), this,  SLOT (set7007_on()));
  connect(ui->lecroy_7008_on, SIGNAL (clicked()), this,  SLOT (set7008_on()));

  connect(ui->lecroy_7005_off, SIGNAL (clicked()), this,  SLOT (set7005_off()));
  connect(ui->lecroy_7006_off, SIGNAL (clicked()), this,  SLOT (set7006_off()));
  connect(ui->lecroy_7007_off, SIGNAL (clicked()), this,  SLOT (set7007_off()));
  connect(ui->lecroy_7008_off, SIGNAL (clicked()), this,  SLOT (set7008_off()));

  // fpd switch control buttons
  connect(ui->fpd_7005_on, SIGNAL (clicked()), this,  SLOT (set7005fpd_on()));
  connect(ui->fpd_7006_on, SIGNAL (clicked()), this,  SLOT (set7006fpd_on()));
  connect(ui->fpd_7007_on, SIGNAL (clicked()), this,  SLOT (set7007fpd_on()));
  connect(ui->fpd_7008_on, SIGNAL (clicked()), this,  SLOT (set7008fpd_on()));
  connect(ui->fpd_6V_on, SIGNAL (clicked()), this,  SLOT (set6V_on()));
  connect(ui->fpd_9V_on, SIGNAL (clicked()), this,  SLOT (set9V_on()));
  connect(ui->fpd_30V_on, SIGNAL (clicked()), this,  SLOT (set30V_on()));

  connect(ui->fpd_7005_off, SIGNAL (clicked()), this,  SLOT (set7005fpd_off()));
  connect(ui->fpd_7006_off, SIGNAL (clicked()), this,  SLOT (set7006fpd_off()));
  connect(ui->fpd_7007_off, SIGNAL (clicked()), this,  SLOT (set7007fpd_off()));
  connect(ui->fpd_7008_off, SIGNAL (clicked()), this,  SLOT (set7008fpd_off()));
  connect(ui->fpd_6V_off, SIGNAL (clicked()), this,  SLOT (set6V_off()));
  connect(ui->fpd_9V_off, SIGNAL (clicked()), this,  SLOT (set9V_off()));
  connect(ui->fpd_30V_off, SIGNAL (clicked()), this,  SLOT (set30V_off()));

  // uv controls
  connect(ui->north_uv_on, SIGNAL (clicked()), this, SLOT (setNorthUV_on()));
  connect(ui->north_uv_off, SIGNAL (clicked()), this, SLOT (setNorthUV_off()));

  connect(ui->south_uv_on, SIGNAL (clicked()), this, SLOT (setSouthUV_on()));
  connect(ui->south_uv_off, SIGNAL (clicked()), this, SLOT (setSouthUV_off()));

  connect(ui->update_uv, SIGNAL (clicked()), this, SLOT (setUpdate_uv_status()));

  // panic button
  connect(ui->panic_button, SIGNAL (clicked()), this, SLOT (setPanic_off()));

  // timer signals
  connect(hv_update_timer, SIGNAL (timeout()), this, SLOT (lock_hv()));
  connect(uv_update_timer, SIGNAL (timeout()), this, SLOT (lock_uv()));
  connect(uv_change_timer[kNorth], SIGNAL (timeout()), this, SLOT (ping_north()));
  connect(uv_change_timer[kSouth], SIGNAL (timeout()), this, SLOT (ping_south()));

  // expert control censoring
  connect(censor_timer, SIGNAL (timeout()), this, SLOT (censor_expert_ctrl()));
  connect(ui->uncensor_button, SIGNAL (clicked()), this, SLOT (uncensor_expert_ctrl()));


  // TIMER SETTINGS---------------------------------------------------
  hv_update_timer->setInterval(120*1000); // units: milliseconds
  uv_update_timer->setInterval(120*1000);

  uv_change_timer[kNorth]->setInterval(30*1000);
  uv_change_timer[kSouth]->setInterval(30*1000);
  uv_change_timer[kNorth]->stop();
  uv_change_timer[kSouth]->stop();

  censor_timer->setInterval(60*1000);
  //------------------------------------------------------------------




  // update panels and switches after initializing their hooks
  update_hv_clicked = false;
  update_uv_clicked = false;
  pinging[kNorth] = false;
  pinging[kSouth] = false;
  hv_interlock_status = kLocked;
  uv_interlock_status = kLocked;
  update_interlock_panel();

  // change 4 lecroys simultaneously hook
  change_four = false;

  // hooks for determining when to send experts a text message when hv or uv was turned off/on in total
  // (they have to reach "2" before a message is sent)
  for(int kk=0; kk<2; kk++) {
    envia_hv_mensaje[kk]=0;
    envia_uv_mensaje[kk]=0;
  };


  // set up log file output
  sprintf(logfile_name,"%s/LOGFILE.html",logdir);
  sprintf(exe_beginlog,"%s; ./%s; %s",
    pushdlog,
    "begin_logfile.sh",
    popd
  );
  system(exe_beginlog);



  // print startup log message
  message = QString("");
  print_log(kAction,message);
  message = QString("============================================================");
  print_log(kAction,message);
  message = QString("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    "FMS Controller GUI Started"
                   );
  print_log(kAction,message);
  message = QString("============================================================");
  print_log(kAction,message);
  message = QString("");
  print_log(kAction,message);
  if(simulation_mode) {
    message = QString("-----------------SIMULATION MODE ENABLED ------------------------");
    print_log(kNote,message);
    message = QString("");
    print_log(kNote,message);
  };

};

MainWindow::~MainWindow() {
  delete ui;
};



// HV on / off /update actions ::::::::::::::::::::::::::::::::::::::::
// -- large on / off
void MainWindow::setLarge_on() {
  make_switch_changing(ui->large_hv_on,"*changing*");
  make_switches_disabled(ui->small_hv_on,ui->small_hv_off,"wait");

  message = QString("shift crew turning on\nlarge cells HV");
  print_status(kAction,message);
  
  currently_changing_hv[kLarge] = true;

  change_four = true;
  make_switch_changing(ui->lecroy_7005_on,">");
  make_switch_changing(ui->lecroy_7006_on,">");
  make_switch_changing(ui->lecroy_7007_on,">");
  make_switch_changing(ui->lecroy_7008_on,">");
  
  changeHV(k7005,kOn); // (which_uv argument doesn't matter here since change_four==true)

  change_four = false;


  /*
  message = QString("pausing for %1 seconds to\nwait for voltages to ramp up").arg(QString::number(lecroy_ramp_time));
  print_status(kNote,message);
  delay(lecroy_ramp_time);
  */

  message = QString("done changing HV;\nramping in background\n(takes ~20 seconds)");
  print_status(kNote,message);
  
  currently_changing_hv[kLarge] = false;
  update_interlock_panel();

  envia_hv_mensaje[kOff]=0;
  envia_hv_mensaje[kOn]++;
  message = QString("note- HV now ON");
  if(envia_hv_mensaje[kOn]>1) print_log(kSMSonly,message);
};

void MainWindow::setLarge_off() {
  make_switch_changing(ui->large_hv_off,"*changing*");
  make_switches_disabled(ui->small_hv_on,ui->small_hv_off,"wait");

  message = QString("shift crew turning off\nlarge cells HV");
  print_status(kAction,message);

  currently_changing_hv[kLarge] = true;

  change_four = true;
  make_switch_changing(ui->lecroy_7005_off,"<");
  make_switch_changing(ui->lecroy_7006_off,"<");
  make_switch_changing(ui->lecroy_7007_off,"<");
  make_switch_changing(ui->lecroy_7008_off,"<");
  
  changeHV(k7005,kOff); // (which_uv argument doesn't matter here since change_four==true)

  change_four = false;


  /*
  message = QString("pausing for %1 seconds to\nwait for voltages to ramp down").arg(QString::number(lecroy_ramp_time));
  print_status(kNote,message);
  delay(lecroy_ramp_time);
  */

  message = QString("done changing HV;\nramping in background\n(takes ~20 seconds)");
  print_status(kNote,message);

  currently_changing_hv[kLarge] = false;
  update_interlock_panel();

  envia_hv_mensaje[kOn]=0;
  envia_hv_mensaje[kOff]++;
  message = QString("note- HV now OFF");
  if(envia_hv_mensaje[kOff]>1) print_log(kSMSonly,message);
};

// --- small on / off
void MainWindow::setSmall_on() {
  make_switch_changing(ui->small_hv_on,"*changing*");
  make_switches_disabled(ui->large_hv_on,ui->large_hv_off,"wait");

  message = QString("shift crew turning on\nsmall cells HV");
  print_status(kAction,message);

  currently_changing_hv[kSmall] = true;

  if(hv_status[k6V]==kOff || fpd_status[k6V]==kOff) { 
    set6V_on(); 
    message = QString("5 second pause...");
    print_status(kNote,message);
    delay(5); 
  };

  if(hv_status[k9V]==kOff || fpd_status[k9V]==kOff) { 
    set9V_on(); 
    message = QString("15 second pause...");
    print_status(kNote,message);
    delay(15); 
  };

  set30V_on(); 
  message = QString("5 second pause...");
  print_status(kNote,message);
  delay(5);

  message = QString("done changing HV");
  print_status(kNote,message);

  currently_changing_hv[kSmall] = false;
  update_interlock_panel();

  envia_hv_mensaje[kOff]=0;
  envia_hv_mensaje[kOn]++;
  message = QString("note- HV now ON");
  if(envia_hv_mensaje[kOn]>1) print_log(kSMSonly,message);
};

void MainWindow::setSmall_off() {
  make_switch_changing(ui->small_hv_off,"*changing*");
  make_switches_disabled(ui->large_hv_on,ui->large_hv_off,"wait");

  message = QString("shift crew turning off\nsmall cells HV");
  print_status(kAction,message);

  currently_changing_hv[kSmall] = true;

  set30V_off();
  message = QString("5 second pause...");
  print_status(kNote,message);
  delay(5);

  message = QString("done changing HV");
  print_status(kNote,message);

  currently_changing_hv[kSmall] = false;
  update_interlock_panel();

  envia_hv_mensaje[kOn]=0;
  envia_hv_mensaje[kOff]++;
  message = QString("note- HV now OFF");
  if(envia_hv_mensaje[kOff]>1) print_log(kSMSonly,message);
};

// -- update all
void MainWindow::setUpdate_hv_status() {
  update_hv_clicked = true;

  if(hv_update_only != uFPD) change_large_lights(kUpdating);
  if(hv_update_only != uLecroy) change_small_lights(kUpdating);

  make_switch_changing(ui->update_hv,"*updating*");

  // print status message
  if(hv_update_only == uAll) {
    message = QString("shift crew updating\nHV status lights...");
    print_status(kUpdateMessage,message);
  } else {
    message = QString("updating HV status lights...");
    print_status(kNote,message);
  };

  if(simulation_mode) {
    delay(2);
  } else {
    delay(1); // obligatory delay so log messages print before backend script executed

    // check lecroy crates ----------------------------------------
    if(hv_update_only == uAll || hv_update_only == uLecroy) {

      // fast status check -----------
      // if hv_update_only==uLecroy, that means we just ran changeHV, and we already have a new 
      // .tab file to read the status from; thus we don't waste time re-making one; 
      // this is done by calling execute_script with do_it=true if hv_update_only is uAll
      strcpy(scriptname,"lecroy_crate_status.deploy.sh"); 
      strcpy(outname,"lecroy_status.tab");
      execute_script(hv_update_only==uAll);
      
      std::ifstream infile_lecroy(outname);
      while(infile_lecroy >> buffer[0] >> buffer[1]) {

        switch(buffer[1]) {
          case 0: 
            message = QString("----- crate %1 is OFF").arg(buffer[0]);
            print_log(kDebug,message);
            new_hv_status = kOff;
            break;
          case 1: 
            message = QString("----- crate %1 is ON").arg(buffer[0]);
            print_log(kDebug,message);
            new_hv_status = kOn;
            break;
          case 4: 
            message = QString("----- crate %1 is RAMPING").arg(buffer[0]);
            print_log(kDebug,message);
            new_hv_status = kRamping;
            break;
          case 8: 
            message = QString("----- crate %1 has unknown status;"
                              " is Lecroy crate physically off? CONTACT EXPERT!").arg(buffer[0]);
            print_log(kError,message);
            new_hv_status = kUnknown;
            break;
          case 9:
            message = QString("----- crate %1 inaccessible because of TELNET CONFLICT!").arg(buffer[0]);
            print_log(kError,message);
            new_hv_status = kUnknown;
            break;
          default:
            message = QString("----- crate %1 REPORTED UNKNOWN STATUS VIA BACKEND; CONTACT EXPERT!").arg(buffer[0]);
            print_log(kCritical,message);
            new_hv_status = kUnknown;
            break;
        };

        switch(buffer[0]) {
          case 7005: hv_status[k7005] = new_hv_status; break;
          case 7006: hv_status[k7006] = new_hv_status; break;
          case 7007: hv_status[k7007] = new_hv_status; break;
          case 7008: hv_status[k7008] = new_hv_status; break;
        };
      }; // eo while infile_lecroy stream


      // slow voltage readback check  -----------
      // (only performed if the Update button was clicked, i.e., if hv_update_only==uAll)
      if(hv_update_only == uAll) {

        message = QString("now checking Large Cell read-backs\nPlease wait...");
        print_status(kNote,message);
        delay(1); // obligatory

        strcpy(scriptname,"lecroy_crate_read.deploy.sh"); 
        strcpy(outname,"lecroy_read.tab");
        execute_script(true);

        std::ifstream infile_lecroy_read(outname);
        while(infile_lecroy_read >> buffer[0] >> ave_volt) {
          switch(buffer[0]) {
            case 7005: this_crate = k7005; break;
            case 7006: this_crate = k7006; break;
            case 7007: this_crate = k7007; break;
            case 7008: this_crate = k7008; break;
            default: this_crate = -1; // cause seg fault
          };

          if(hv_status[this_crate]==kOff && ave_volt > voltage_threshold) {
            message = QString("%1 is %2 but average voltage is above threshold (%3 V > %4 V); marking as ON; consider calling EXPERT").arg(
              hv_name[this_crate],state_name[hv_status[this_crate]],
              QString::number(ave_volt,'f',2),
              QString::number(voltage_threshold,'f',1)
            );
            hv_status[this_crate]=kOn;
            print_log(kError,message);
          } else {
            if(hv_status[this_crate]!=kUnknown) {
              message = QString("%1 is %2 and average voltage is %3 V").arg(
                hv_name[this_crate],state_name[hv_status[this_crate]],
                QString::number(ave_volt,'f',2)
              );
              print_log(kNote,message);
            } else {
              message = QString("%1 is %2 and average voltage is %3").arg(
                hv_name[this_crate],state_name[hv_status[this_crate]],
                state_name[hv_status[this_crate]]
              );
              print_log(kError,message);
            };
          };
        };
      };
          


    }; // eo if hv_update_only is all or lecroy


    // check fpd switches ------------
    if(hv_update_only == uAll || hv_update_only == uFPD) {

      // if hv_update_only==uFPD, that means we just ran changeHV, and we already have a new 
      // .tab file to read the status from; thus we don't waste time re-making one; 
      // this is done by calling execute_script with do_it=true if hv_update_only is uAll
      strcpy(scriptname,"fpd_switch_status.deploy.sh"); 
      strcpy(outname,"fpd_status.tab");
      execute_script(hv_update_only==uAll);
      
      std::ifstream infile_fpd(outname);
      while(infile_fpd >> buffer[0] >> buffer[1]) {

        switch(buffer[1]) {
          case 0: 
            message = QString("----- %1 switch is OFF").arg(buffer[0]);
            print_log(kDebug,message);
            new_hv_status = kOff;
            break;
          case 1: 
            message = QString("----- %1 switch is ON").arg(buffer[0]);
            print_log(kDebug,message);
            new_hv_status = kOn;
            break;
          case 8: 
            message = QString("----- %1 switch has unknown status; CONTACT EXPERT!").arg(buffer[0]);
            print_log(kError,message);
            new_hv_status = kUnknown;
            break;
          case 9: 
            message = QString("----- %1 switch inaccessible because of TELNET CONFLICT!").arg(buffer[0]);
            print_log(kError,message);
            new_hv_status = kUnknown;
            break;
          default:
            message = QString("----- %1 SWITCH REPORTED UNKNOWN STATUS VIA BACKEND; CONTACT EXPERT!").arg(buffer[0]);
            print_log(kCritical,message);
            new_hv_status = kUnknown;
            break;
        };

        switch(buffer[0]) {
          case 7005: fpd_status[k7005] = new_hv_status; break;
          case 7006: fpd_status[k7006] = new_hv_status; break;
          case 7007: fpd_status[k7007] = new_hv_status; break;
          case 7008: fpd_status[k7008] = new_hv_status; break;
          case 30: 
            fpd_status[k30V] = new_hv_status;
            hv_status[k30V] = new_hv_status;
            break;
          case 9: 
            fpd_status[k9V] = new_hv_status;
            hv_status[k9V] = new_hv_status;
            break;
          case 6: 
            fpd_status[k6V] = new_hv_status;
            hv_status[k6V] = new_hv_status;
            break;
        };
      }; // eo while infile_fpd stream

    }; // eo if hv_update_only all or fpd

  }; // eo if !simulation_mode

  update_interlock_panel();

  ui->update_hv->setStyleSheet("background-color: rgb(255,85,255); color: black");
  ui->update_hv->setText("Update HV Status");
  ui->update_hv->setDisabled(false);

  if(!major_problem) {
    message = QString("HV Status lights updated");
    print_status(kNote,message);
  };
  update_hv_clicked = false;
};

void MainWindow::setUpdate_uv_status() {
  update_uv_clicked = true;
  change_uv_lights(kUpdating);
  make_switch_changing(ui->update_uv,"*updating*");

  // print status message
  if(uv_update_only == uAll) {
    message = QString("shift crew updating\nUV status lights\nand bank currents...");
    print_status(kUpdateMessage,message);
  } else {
    message = QString("updating UV status lights\nand bank currents...");
    print_status(kNote,message);
  };

  if(uv_update_only==uAll || uv_update_only==uNorth) {
    if(!pinging[kNorth]) {
      ui->bank1_north_status_bar->setValue(0); 
      ui->bank2_north_status_bar->setValue(0); 
      ui->bank3_north_status_bar->setValue(0); 
      ui->bank1_north_status_bar->setStyleSheet("background-color: rgb(180,180,180);");
      ui->bank2_north_status_bar->setStyleSheet("background-color: rgb(180,180,180);");
      ui->bank3_north_status_bar->setStyleSheet("background-color: rgb(180,180,180);");
      ui->bank1_north_status->setText("wait");
      ui->bank2_north_status->setText("wait");
      ui->bank3_north_status->setText("wait");
      ui->north_power->setText("wait");
    };
  };
  if(uv_update_only==uAll || uv_update_only==uSouth) {
    if(!pinging[kSouth]) {
      ui->bank1_south_status_bar->setValue(0); 
      ui->bank2_south_status_bar->setValue(0); 
      ui->bank3_south_status_bar->setValue(0); 
      ui->bank1_south_status_bar->setStyleSheet("background-color: rgb(180,180,180);");
      ui->bank2_south_status_bar->setStyleSheet("background-color: rgb(180,180,180);");
      ui->bank3_south_status_bar->setStyleSheet("background-color: rgb(180,180,180);");
      ui->bank1_south_status->setText("wait");
      ui->bank2_south_status->setText("wait");
      ui->bank3_south_status->setText("wait");
      ui->south_power->setText("wait");
    };
  };


  if(simulation_mode) {
    delay(2);
  } else {
    delay(1); // necessary delay so log messages print before backend script executed

    // loop over both PDUs
    for(int this_pdu=kNorth; this_pdu<=kSouth; this_pdu++) {

      // check only single PDU if we want, using uv_update_only hook
      if(uv_update_only==uAll || (uv_update_only==uNorth && this_pdu==kNorth) || 
                                 (uv_update_only==uSouth && this_pdu==kSouth)) {
         
        // only execute the script if the update uv button was clicked, that is
        // only if uv_update_only==uAll and if pinging==false; in this case, the
        // uvled_status_both script is called, which sends status request to both sides
        // simultaneously. We only want to call this script once in this for loop, so
        // we call it only when this_pdu==kNorth; (the second time around is used to 
        // change the output file name to the south side)
        //
        // if we are here with uv_update_only!=uAll and pinging==true and ping_cnt==0
        // that means we just sent a 'change' to one of the PDUs, and the relevant 
        // status output file has already been generated, so there's no need to 
        // re-run the bash script
        // 
        // but if we are here with uv_update_only!=uAll and pinging==true and ping_cnt>0, 
        // we *do* want to execute the backend script! ...sorry this is mad confusing.... :)
        //
        if(pinging[this_pdu]) {
          sprintf(scriptname,"uvled_status.deploy.sh %s",pdu_name_short[this_pdu]); 
          sprintf(outname,"uvled_%s_status.tab",pdu_name_short[this_pdu]);
          if(ping_cnt[this_pdu]==0) execute_script(uv_update_only==uAll);
          else execute_script(true);
        } else {
          sprintf(scriptname,"uvled_status_both.deploy.sh"); 
          sprintf(outname,"uvled_%s_status.tab",pdu_name_short[this_pdu]);
          execute_script(this_pdu==kNorth); // (only run for 1st iteration of loop; 2nd time just set filenames)
        };

        std::ifstream infile_uvled(outname);
        while(infile_uvled >> buffer[0] >> buffer[1]) {
          // set outlet statuses
          if(buffer[0]<100) {
            switch(buffer[1]) {
              case 0: new_uv_status = kOff; break;
              case 1: new_uv_status = kOn; break;
              case 2: new_uv_status = kUpdating; break;
              case 9: new_uv_status = kUnknown; break;
              default: new_uv_status = kUnknown; break;
            };

            switch(buffer[0]) {
              case 5: uv_status[this_pdu][kBottom][kps2] = new_uv_status; break;
              case 6: uv_status[this_pdu][kBottom][kps1] = new_uv_status; break;
              case 7: uv_status[this_pdu][kTop][kps2] = new_uv_status; break;
              case 8: uv_status[this_pdu][kTop][kps1] = new_uv_status; break;

              case 13: uv_status[this_pdu][kBottom][kps4] = new_uv_status; break;
              case 14: uv_status[this_pdu][kBottom][kps3] = new_uv_status; break;
              case 15: uv_status[this_pdu][kTop][kps4] = new_uv_status; break;
              case 16: uv_status[this_pdu][kTop][kps3] = new_uv_status; break;

              case 21: uv_status[this_pdu][kBottom][kps6] = new_uv_status; break;
              case 22: uv_status[this_pdu][kBottom][kps5] = new_uv_status; break;
              case 23: uv_status[this_pdu][kTop][kps6] = new_uv_status; break;
              case 24: uv_status[this_pdu][kTop][kps5] = new_uv_status; break;

              default: break;
            };
          } // eo if buffer[0] is outlet

          // bank currents
          else if(buffer[0]>=100 && buffer[0]<200) {
            switch(buffer[0]) {
              case 101: this_bank = kB1; break;
              case 102: this_bank = kB2; break;
              case 103: this_bank = kB3; break;
              default: this_bank = -1; break;
            }

            if(this_bank<0) {
              message = QString("unknown bank number!\nuvled currents may be wrong!");
              print_status(kCritical,message);
            } else {
              bank_current[this_pdu][this_bank] = ((float)buffer[1])/10.0;
              if(buffer[1]<0) {
                message = QString("Could not read %1 PDU\n%2 current").arg(
                  pdu_name[this_pdu],
                  bank_name[this_bank]
                );
                print_log(kError,message);
              };
            };
          } // eo if buffer[0] is bank current

          // read powers (in watts and V-A, which shouldn't be different, but
          // APC provides both for some crazy reason)
          else if(buffer[0]>=200 && buffer[0]<300) {
            switch(buffer[0]) {
              case 201: these_power_units = kVA; break;
              case 202: these_power_units = kWatts; break;
              default: these_power_units = -1; break;
            }

            if(these_power_units<0) {
              message = QString("unknown power units!\nthis should never happen...");
              print_log(kError,message);
            } else {
              pdu_power[this_pdu][these_power_units] = ((float)buffer[1])/10.0;
              if(buffer[1]<0) {
                message = QString("Could not read %1 PDU power").arg(pdu_name[this_pdu]);
                print_log(kError,message);
              };
            };

          }; // eo if buffer[0] is power
        }; // eo while infile_uvled
      }; // eo if uv_update_only is ...
    }; // eo for loop over north & south PDUs
  }; // eo if !simulation_mode

  ui->update_uv->setStyleSheet("background-color: rgb(255,85,255); color: black");
  ui->update_uv->setText("Update UV Status");
  ui->update_uv->setDisabled(false);

  update_interlock_panel();

  if(!major_problem) {
    message = QString("UV Status lights updated");
    print_status(kNote,message);
  };

  check_uv_critical();
  update_uv_clicked = false;
};


// "ping" updates for PDU's, which are called by a timer while a switch is changing
void MainWindow::ping_north() { ping_pdu(kNorth); };
void MainWindow::ping_south() { ping_pdu(kSouth); };

void MainWindow::ping_pdu(int which_pdu) {
  delay(1); // obligatory

  // 1. come in with ping_cnt = 0; increment to 1 --> restart timer --> update
  // 2. come in with ping_cnt = 1; increment to 2 --> stop timer --> update --> set pinging to false
  
  ping_cnt[which_pdu]++;

  message = QString("Update ping %1 sent to %2 PDU").arg(
    QString::number(ping_cnt[which_pdu]),
    pdu_name[which_pdu]);
  print_log(kNote,message);


  if(ping_cnt[which_pdu]<2) uv_change_timer[which_pdu]->start();
  else uv_change_timer[which_pdu]->stop();

  switch(which_pdu) {
    case kNorth: uv_update_only = uNorth; break;
    case kSouth: uv_update_only = uSouth; break;
  };

  setUpdate_uv_status();
  uv_update_only = uAll;
  if(ping_cnt[which_pdu]>1) {
    pinging[which_pdu] = false;
    update_uv_panel();
  };
};






// HV switch actions ::::::::::::::::::::::::::::::::::::::::
void MainWindow::set7005_on() { make_switch_changing(ui->lecroy_7005_on,">"); changeHV(k7005, kOn); };
void MainWindow::set7006_on() { make_switch_changing(ui->lecroy_7006_on,">"); changeHV(k7006, kOn); };
void MainWindow::set7007_on() { make_switch_changing(ui->lecroy_7007_on,">"); changeHV(k7007, kOn); };
void MainWindow::set7008_on() { make_switch_changing(ui->lecroy_7008_on,">"); changeHV(k7008, kOn); };

void MainWindow::set7005_off() { make_switch_changing(ui->lecroy_7005_off,"<"); changeHV(k7005, kOff); };
void MainWindow::set7006_off() { make_switch_changing(ui->lecroy_7006_off,"<"); changeHV(k7006, kOff); };
void MainWindow::set7007_off() { make_switch_changing(ui->lecroy_7007_off,"<"); changeHV(k7007, kOff); };
void MainWindow::set7008_off() { make_switch_changing(ui->lecroy_7008_off,"<"); changeHV(k7008, kOff); };

void MainWindow::set7005fpd_on() { make_switch_changing(ui->fpd_7005_on,">"); changeLecroyFpd(k7005, kOn); };
void MainWindow::set7006fpd_on() { make_switch_changing(ui->fpd_7006_on,">"); changeLecroyFpd(k7006, kOn); };
void MainWindow::set7007fpd_on() { make_switch_changing(ui->fpd_7007_on,">"); changeLecroyFpd(k7007, kOn); };
void MainWindow::set7008fpd_on() { make_switch_changing(ui->fpd_7008_on,">"); changeLecroyFpd(k7008, kOn); };

void MainWindow::set7005fpd_off() { make_switch_changing(ui->fpd_7005_off,"<"); changeLecroyFpd(k7005, kOff); };
void MainWindow::set7006fpd_off() { make_switch_changing(ui->fpd_7006_off,"<"); changeLecroyFpd(k7006, kOff); };
void MainWindow::set7007fpd_off() { make_switch_changing(ui->fpd_7007_off,"<"); changeLecroyFpd(k7007, kOff); };
void MainWindow::set7008fpd_off() { make_switch_changing(ui->fpd_7008_off,"<"); changeLecroyFpd(k7008, kOff); };

void MainWindow::set30V_on() { make_switch_changing(ui->fpd_30V_on,">"); changeHV(k30V, kOn); };
void MainWindow::set9V_on() { make_switch_changing(ui->fpd_9V_on,">"); changeHV(k9V, kOn); };
void MainWindow::set6V_on() { make_switch_changing(ui->fpd_6V_on,">"); changeHV(k6V, kOn); };

void MainWindow::set30V_off() { make_switch_changing(ui->fpd_30V_off,"<"); changeHV(k30V, kOff); };
void MainWindow::set9V_off() { make_switch_changing(ui->fpd_9V_off,"<"); changeHV(k9V, kOff); };
void MainWindow::set6V_off() { make_switch_changing(ui->fpd_6V_off,"<"); changeHV(k6V, kOff); };

void MainWindow::setNorthUV_on()  { 
  changeUV(kNorth, kOn,  false); 
  envia_uv_mensaje[kOff]=0;
  envia_uv_mensaje[kOn]++;
  message = QString("note- UVleds now ON");
  if(envia_uv_mensaje[kOn]>1) print_log(kSMSonly,message);
};
void MainWindow::setSouthUV_on()  { 
  changeUV(kSouth, kOn,  false); 
  envia_uv_mensaje[kOff]=0;
  envia_uv_mensaje[kOn]++;
  message = QString("note- UVleds now ON");
  if(envia_uv_mensaje[kOn]>1) print_log(kSMSonly,message);
};
void MainWindow::setNorthUV_off() { 
  changeUV(kNorth, kOff, false); 
  envia_uv_mensaje[kOn]=0;
  envia_uv_mensaje[kOff]++;
  message = QString("note- UVleds now OFF");
  if(envia_uv_mensaje[kOff]>1) print_log(kSMSonly,message);
};
void MainWindow::setSouthUV_off() { 
  changeUV(kSouth, kOff, false); 
  envia_uv_mensaje[kOn]=0;
  envia_uv_mensaje[kOff]++;
  message = QString("note- UVleds now OFF");
  if(envia_uv_mensaje[kOff]>1) print_log(kSMSonly,message);
};

void MainWindow::setPanic_off() {
  make_switch_changing(ui->panic_button,"wait...");

  if(!major_problem) {
    message = QString("PANIC OFF HAS\nBEEN REQUESTED!");
    print_status(kCritical,message);
  };
  delay(1);

  // calling changeUV with panic==true sends panic of to BOTH PDUs; 
  // no need to also send a request to the south side
  changeUV(kNorth, kOff, true); 

  // also send "off" command to high voltage
  message = QString("PANIC ACTION:\nturning OFF High Voltage...");
  print_log(kAction,message);
  // small cells first (since there are more uvLEDs there)
   fpd_status[k30V] = kOn; hv_status[k30V] = kOn; // (override statuses for safety)
   fpd_status[k9V] = kOn; hv_status[k9V] = kOn;
   fpd_status[k6V] = kOn; hv_status[k6V] = kOn;
  setSmall_off();
  // then do large cells
  int lecroy_ramp_time_old = lecroy_ramp_time;
  lecroy_ramp_time = 0; // override ramping time
   fpd_status[k7005] = kOn; // (override fpd switch statuses for safety)
   fpd_status[k7006] = kOn; 
   fpd_status[k7007] = kOn; 
   fpd_status[k7008] = kOn;
  setLarge_off();
  lecroy_ramp_time = lecroy_ramp_time_old;

  // now set all statuses to unknown, so the operator must click "update"
  // (unless we're using the testing overrides; see initialize_statuses method)
  initialize_statuses();

  if(!major_problem) {
    message = QString("HV and UV powered down.\nCALL FMS EXPERT");
    print_status(kCritical,message);

    update_interlock_panel();
    
  } else {
    lock_hv();
    lock_uv();
    ui->interlock_status_message->setText("<font color=\"Red\">-- PANIC MODE LOCKOUT --</font>");/// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111 add manual control only line
    change_large_lights(kUnknown);
    change_small_lights(kUnknown);
    change_uv_lights(kUnknown);
    delay(1);
    message = QString("HV and UV auto-powered down.\nCALL FMS EXPERT IMMEDIATELY");
    print_status(kCritical,message);
    for(int xx=0; xx<10; xx++) {
      delay(1);
      print_status(kAction,message);
      delay(1);
      print_status(kNote,message);
    };
  };


  ui->panic_button->setStyleSheet("background-color: rgb(255,255,0); color: black");
  ui->panic_button->setText("PANIC");
  ui->panic_button->setDisabled(false);
};



// general HV switch -- only THIS method can *actually* chang HV
void MainWindow::changeHV(int which_hv, int setting) {
  
  // fast lock the UVleds on/off controls if we're turning on a HV
  if(setting==kOn) lock_uv();

  // double-check interlock
  if(setting==kOn && check_overall_uv_state()==kOn) {
    message = QString("Interlock Problem:\ncannot turn HV on with UV on");
    print_status(kCritical,message);
    message = QString("---- interlock problem caught by changeHV method");
    print_log(kCritical,message);
    update_interlock_panel();
    return;
  };

  if(change_four==true && 
     (which_hv==k7005 || which_hv==k7006 || which_hv==k7007 || which_hv==k7008)) {
    message = QString("requesting to change\nall Lecroy crates to %1").arg(state_name[setting]);
  } else {
    message = QString("requesting to change\n%1 to %2").arg(hv_name[which_hv],state_name[setting]);
  };
  if(currently_changing_hv[kLarge] || currently_changing_hv[kSmall]) 
    print_log(kAction,message);
  else
    print_status(kAction,message);



  bool allowed = false; // (for small cells hv only)
  error_occured = false; // if error occurs during change, set to true and don't run setUpdate_hv_status

  if(simulation_mode) {
    simulate_changeHV(which_hv,setting);
  } else {
    
    delay(1); // obligatory

    if(which_hv==k7005 || which_hv==k7006 || which_hv==k7007 || which_hv==k7008) {
      // shift crew large cells on/off (i.e., currently_changing_hv[kLarge]==true)
      // 1. call lecroy_crate_change4 backend script, which just sends on/off command to ALL 4 crates
      //    then 'show status'; outputs to status file
      // 2. calls setUpdate_hv_status with hv_update_only = uLecroy, which only checks status from this file
      //    and updates the lights
      // 3. has a delay set after the last call, in order to wait for ramp
      // 4. does not do voltage read-back test; only pressing the update button will do that
      //
      // expert lecroy on/off (i.e., currently_changing_hv[kLarge]==false)
      // * same as above but does not include the ramping wait; instead just prints that expert switch
      //   was used and that the crate is 'ramping in the background'; this is so that control is returned
      //   to the operator as quickly as possible


      // shift crew call
      if(change_four) {
        if(fpd_status[k7005]==kOn && fpd_status[k7006]==kOn && 
           fpd_status[k7007]==kOn && fpd_status[k7008]==kOn) {

          sprintf(scriptname,"lecroy_crate_change4.deploy.sh %s",
            state_name_short[setting]
          );
          strcpy(outname,"lecroy_status.tab");
          execute_script(true);

        } else {
          message = QString("cannot change lecroy crates\nwith FPD switch off or unknown");
          print_status(kError,message);
          error_occured = true;
        };
      }

      // expert call
      else if(!change_four) {
        if(fpd_status[which_hv] == kOn) {

          sprintf(scriptname,"lecroy_crate_change.deploy.sh %s %s",
            hv_name_short[which_hv],
            state_name_short[setting]
          );
          strcpy(outname,"lecroy_status.tab");
          execute_script(true);

        } else { 
          message = QString("cannot change %1\nwith FPD switch off or unknown").arg(hv_name[which_hv]);
          print_status(kError,message);
          error_occured = true;
        };
      };

      // print status update (if no error occured)
      if(!error_occured) {
        if(currently_changing_hv[kLarge]) 
          message = QString("Large Cells\nchanged to %1").arg(state_name[setting]);
        else
          message = QString("%1\nchanged to %2\n- ramping in background -").arg(hv_name[which_hv],state_name[setting]);
        print_status(kAction,message);

        hv_update_only = uLecroy;
      };
               
    } // eo if which_hv is large cell hv

    else if(which_hv==k30V || which_hv==k9V || which_hv==k6V) {
      // shift crew small cells on/off (i.e., currently_changing_hv[kLarge]==true)
      // 1. call fpd_switch_change backend script, which just sends on/off command and outputs status file
      // 2. calls setUpdate_hv_status with hv_update_only = uFPD, which will just use that output file for the status check
      //
      // expert small cells on/off (i.e., currently_changing_hv[kLarge]==false)
      // * same as shift crew control calls, since status readback is fast anyway

      switch(which_hv) {
        case k30V:
          if( hv_status[k9V]  == kOn  && hv_status[k6V]  == kOn  &&
              fpd_status[k9V] == kOn  && fpd_status[k6V] == kOn  ) allowed = true;
          break;
        case k9V:
          if(hv_status[k30V]  == kOff && hv_status[k6V]  == kOn  &&
             fpd_status[k30V] == kOff && fpd_status[k6V] == kOn  ) allowed = true;
          break;
        case k6V:
          if(hv_status[k30V]  == kOff && hv_status[k9V]  == kOff &&
             fpd_status[k30V] == kOff && fpd_status[k9V] == kOff ) allowed = true;
          break;
      };
      if(allowed) { 
        sprintf(scriptname,"fpd_switch_change.deploy.sh %s %s",
          hv_name_short[which_hv],
          state_name_short[setting]
        ); 
        strcpy(outname,"fpd_status.tab");
        execute_script(true);

        message = QString("Small Cells %1\nchanged to %2").arg(hv_name[which_hv],state_name[setting]);
        print_status(kAction,message);

        hv_update_only = uFPD;
        
      } else {
        message = QString("Small Cell HV/LV cannot\nbe switched on/off out of order");
        print_status(kError,message);
        error_occured = true;
      };
    } // eo elseif which_hv is small cell hv

  }; // eo if !simulation_mode
     

  if(!error_occured) setUpdate_hv_status();
  else update_hv_panel();
  hv_update_only = uAll;
};

void MainWindow::changeLecroyFpd(int which_hv, int setting) {

  if(simulation_mode) {
    simulate_changeLecroyFpd(which_hv,setting);
  } else {
    //system("backend/blah.sh");
  };

  update_interlock_panel();
};



// general UV switch -- only THIS method can *actually* chang UVleds
void MainWindow::changeUV(int which_pdu, int setting, bool panic) {

  // fast lock the HV on/off controls if we're turning on the UVleds
  if(setting==kOn) lock_hv();

  // first print out status message
  message = QString("shift crew turning\n%1 %2 uvLEDs").arg(
    state_name[setting],pdu_name[which_pdu]
  );
  print_status(kAction,message);
  message = QString("%1 uvLEDs before turning %2:\n --- B1: %3A --- B2: %4A\n --- B3: %5A ---").arg(
    pdu_name[which_pdu],state_name[setting],
    QString::number(bank_current[which_pdu][kB1],'f',1),
    QString::number(bank_current[which_pdu][kB2],'f',1),
    QString::number(bank_current[which_pdu][kB3],'f',1)
  );
  print_log(kNote,message);

  // double-check interlock
  if(setting==kOn && check_overall_hv_state()==kOn) {
    message = QString("Interlock Problem:\ncannot turn UV on with HV on");
    print_status(kCritical,message);
    message = QString("---- interlock problem caught by changeUV method");
    print_log(kCritical,message);
    update_interlock_panel();
    return;
  };

  currently_changing_uv[which_pdu] = true;

  // make the clicked switch say "changing"
  QPushButton * which_button;
  switch(which_pdu) {
    case kNorth:
      switch(setting) {
        case kOn:  which_button = ui->north_uv_on;  break;
        case kOff: which_button = ui->north_uv_off; break;
      };
      break;
    case kSouth:
      switch(setting) {
        case kOn:  which_button = ui->south_uv_on;  break;
        case kOff: which_button = ui->south_uv_off; break;
      };
      break;
  };
  make_switch_changing(which_button,"*changing*");


  // disable other power switch, printing the word "wait"
  switch(which_pdu) {
    case kNorth: make_switches_disabled(ui->south_uv_on,ui->south_uv_off,"wait"); break;
    case kSouth: make_switches_disabled(ui->north_uv_on,ui->north_uv_off,"wait"); break;
  };


  if(simulation_mode) {
    simulate_changeUV(which_pdu,setting,panic);
  } else {

    delay(1); // obligatory

    if(panic) {
      message = QString("PANIC ACTION:\nturning %1 uvLEDs...").arg(state_name[setting]);
      print_log(kAction,message);

      sprintf(scriptname,"uvled_panic.deploy.sh");
      sprintf(outname,"uvled_%s_status.tab",pdu_name_short[which_pdu]);
      execute_script(true);

    } else {
      //
      // shift crew uvleds on/off
      // 1. call backend script
      // 2. let it run in the backround (it just calls delayed{on,off}, which runs on the PDU
      //    after the console session is closed)
      // 3. uvled lights are immediately updated and a timer unique to changing pdu is started
      // 4. uvled lights for this pdu are updated ("pinged") every ~30 seconds, a total of 2 more times

      // call backend
      sprintf(scriptname,"uvled_change.deploy.sh %s %s",
        pdu_name_short[which_pdu],
        state_name_short[setting]
      );
      sprintf(outname,"uvled_%s_status.tab",pdu_name_short[which_pdu]);
      execute_script(true);

      // print status message
      message = QString("%1 command sent to\n%2 PDU").arg(state_name[setting],pdu_name[which_pdu]);
      print_status(kNote,message);

      // set update_only hook
      switch(which_pdu) {
        case kNorth: uv_update_only = uNorth; break;
        case kSouth: uv_update_only = uSouth; break;
      };

      // start the ping timer and counter
      pinging[which_pdu] = true;
      ping_cnt[which_pdu] = 0;
      uv_change_timer[which_pdu]->start();
        
    }; // eo if !panic

  }; // eo if !simulation_mode
    
  currently_changing_uv[which_pdu] = false;

  message = QString("%1 uvLEDs now %2:\n --- B1: %3A --- B2: %4A\n --- B3: %5A ---").arg(
    pdu_name[which_pdu],state_name[setting],
    QString::number(bank_current[which_pdu][kB1],'f',1),
    QString::number(bank_current[which_pdu][kB2],'f',1),
    QString::number(bank_current[which_pdu][kB3],'f',1)
  );
  print_status(kNote,message);

  // update uv statuses (for which_pdu PDU only)
  setUpdate_uv_status();
  uv_update_only = uAll;
};


// hv check section :::::::::::::::::::::::::::::::::::::::::::
int MainWindow::check_large_hv_for_switches() {
  if(currently_changing_hv[kLarge]) return kNotImportant;
  if(      hv_status[k7005]==kUnknown ||
           hv_status[k7006]==kUnknown ||
           hv_status[k7007]==kUnknown ||
           hv_status[k7008]==kUnknown
  ) return kUnknown;
  else if( hv_status[k7005]==kRamping ||
           hv_status[k7006]==kRamping ||
           hv_status[k7007]==kRamping ||
           hv_status[k7008]==kRamping
  ) return kRamping;
  else if( fpd_status[k7005]==kOff ||
           fpd_status[k7006]==kOff ||
           fpd_status[k7007]==kOff ||
           fpd_status[k7008]==kOff
  ) return kExpert;
  else if( hv_status[k7005]==kOn &&
           hv_status[k7006]==kOn &&
           hv_status[k7007]==kOn &&
           hv_status[k7008]==kOn
  ) return kOn;
  else if( hv_status[k7005]==kOff &&
           hv_status[k7006]==kOff &&
           hv_status[k7007]==kOff &&
           hv_status[k7008]==kOff
  ) return kOff;
  else if( fpd_status[k7005]==kOn &&
           fpd_status[k7006]==kOn &&
           fpd_status[k7007]==kOn &&
           fpd_status[k7008]==kOn
  ) return kNotImportant;

  else return kExpert;
};

int MainWindow::check_small_hv_for_switches() {
  if(currently_changing_hv[kSmall]) return kNotImportant;
  if( hv_status[k30V]==kUnknown ||
      hv_status[k9V]==kUnknown ||
      hv_status[k6V]==kUnknown
  ) return kUnknown;
  else if( hv_status[k30V]==kOn &&
           hv_status[k9V]==kOn &&
           hv_status[k6V]==kOn
  ) return kOn;
  else if( hv_status[k30V]==kOff) return kOff;
  else return kExpert;
};


int MainWindow::check_uv_for_switches(int northsouth) {
  if(currently_changing_uv[northsouth]) return kNotImportant;
  // return "on" if *any* power supply is on
  for(int tt=0; tt<2; tt++) {
    for(int pp=0; pp<kNps; pp++) {
      if(uv_status[northsouth][tt][pp]==kOn) return kOn;
    };
  };
  return kOff;
};


int MainWindow::check_overall_hv_state() {
  int retval = kOff;
  // first check if anything is "ON"
  for(int v=0; v<kNhvs; v++) {
    if(v!=k6V && v!=k9V) {
      if(hv_status[v] == kOn) retval = kOn;
    };
  };
  // then if everything is off, check if anything is "UNKNOWN"
  if(retval != kOn) {
    for(int v=0; v<kNhvs; v++) {
      if(v!=k6V && v!=k9V) {
        if(hv_status[v] == kUnknown) retval = kUnknown;
      };
    };
  };
    
  return retval;
};

int MainWindow::check_overall_uv_state() {
  int retval = kOff;
  // first check if anything is "ON"
  for(int nnn=0; nnn<2; nnn++) {
    // loop through power supplies
    for(int ttt=0; ttt<2; ttt++) {
      for(int ppp=0; ppp<kNps; ppp++) {
        if(uv_status[nnn][ttt][ppp] == kOn) {

          retval = kOn;

          message = QString("check_overall_uv_state -- uv_status[%1][%2][%3] = %4").arg(
            QString::number(nnn),QString::number(ttt),QString::number(ppp),
            QString::number(uv_status[nnn][ttt][ppp]));
          print_log(kDebug,message);
        };
      };
    };
    // double check bank currents
    for(int bbb=0; bbb<3; bbb++) {
      if(bank_current[nnn][bbb]>0.1) {
        
        retval = kOn;

        message = QString("check_overall_uv_state -- bank_current[%1][%2] = %3").arg(
          QString::number(nnn),QString::number(bbb),
          QString::number(bank_current[nnn][bbb]));
        print_log(kDebug,message);
      };
    };
  };
  // then if everything is off, check if anything is "UNKNOWN"
  if(retval != kOn) {
    for(int nnn=0; nnn<2; nnn++) {
      for(int ttt=0; ttt<2; ttt++) {
        for(int ppp=0; ppp<kNps; ppp++) {
          if(uv_status[nnn][ttt][ppp] == kUnknown) retval = kUnknown;
        };
      };
      for(int bbb=0; bbb<3; bbb++) {
        if(bank_current[nnn][bbb]<-0.1) retval = kUnknown;
      };
    };
  };
  return retval;
};


// change hv lights action ::::::::::::::::::::::::::::::::::::
void MainWindow::change_large_lights(int setting) {
  if(setting>=0 && setting<kNstatuses) {
    ui->light_7005->setPixmap(*hv_light_image[k7005][setting]);
    ui->light_7006->setPixmap(*hv_light_image[k7006][setting]);
    ui->light_7007->setPixmap(*hv_light_image[k7007][setting]);
    ui->light_7008->setPixmap(*hv_light_image[k7008][setting]);
  };
};

void MainWindow::change_small_lights(int setting) {
  if(setting>=0 && setting<kNstatuses) {
    ui->light_30V->setPixmap(*hv_light_image[k30V][setting]);
    ui->light_9V->setPixmap(*hv_light_image[k9V][setting]);
    ui->light_6V->setPixmap(*hv_light_image[k6V][setting]);
  };
};

void MainWindow::change_uv_lights(int setting) {
  if(setting>=0 && setting<kNstatuses) {
    if(uv_update_only==uAll || uv_update_only==uNorth) {
      if(!pinging[kNorth]) {
        ui->light_n1t->setPixmap(*uv_light_image[setting]);
        ui->light_n2t->setPixmap(*uv_light_image[setting]);
        ui->light_n3t->setPixmap(*uv_light_image[setting]);
        ui->light_n4t->setPixmap(*uv_light_image[setting]);
        ui->light_n5t->setPixmap(*uv_light_image[setting]);
        ui->light_n6t->setPixmap(*uv_light_image[setting]);

        ui->light_n1b->setPixmap(*uv_light_image[setting]);
        ui->light_n2b->setPixmap(*uv_light_image[setting]);
        ui->light_n3b->setPixmap(*uv_light_image[setting]);
        ui->light_n4b->setPixmap(*uv_light_image[setting]);
        ui->light_n5b->setPixmap(*uv_light_image[setting]);
        ui->light_n6b->setPixmap(*uv_light_image[setting]);
      };
    }

    if(uv_update_only==uAll || uv_update_only==uSouth) {
      if(!pinging[kSouth]) {
        ui->light_s1t->setPixmap(*uv_light_image[setting]);
        ui->light_s2t->setPixmap(*uv_light_image[setting]);
        ui->light_s3t->setPixmap(*uv_light_image[setting]);
        ui->light_s4t->setPixmap(*uv_light_image[setting]);
        ui->light_s5t->setPixmap(*uv_light_image[setting]);
        ui->light_s6t->setPixmap(*uv_light_image[setting]);

        ui->light_s1b->setPixmap(*uv_light_image[setting]);
        ui->light_s2b->setPixmap(*uv_light_image[setting]);
        ui->light_s3b->setPixmap(*uv_light_image[setting]);
        ui->light_s4b->setPixmap(*uv_light_image[setting]);
        ui->light_s5b->setPixmap(*uv_light_image[setting]);
        ui->light_s6b->setPixmap(*uv_light_image[setting]);
      };
    };
  };
};


void MainWindow::update_hv_panel() {
  // shift crew switches
  switch(check_large_hv_for_switches()) {
    case kOn:  flick_switch(ui->large_hv_off, ui->large_hv_on, 255, 255, 50,  kOn);  break;
    case kOff: flick_switch(ui->large_hv_on, ui->large_hv_off, 130, 130, 130, kOff); break;
    case kExpert: 
      make_switches_disabled(ui->large_hv_off, ui->large_hv_on, "expert");
      break;
    case kUnknown: 
      make_switches_disabled(ui->large_hv_off, ui->large_hv_on, "unknown");
      break;
    case kRamping:
      make_switches_disabled(ui->large_hv_off, ui->large_hv_on, "ramping");
      break;
  };
  switch(check_small_hv_for_switches()) {
    case kOn:  flick_switch(ui->small_hv_off, ui->small_hv_on, 255, 255, 50,  kOn);  break;
    case kOff: flick_switch(ui->small_hv_on, ui->small_hv_off, 130, 130, 130, kOff); break;
    case kExpert: 
      make_switches_disabled(ui->small_hv_off, ui->small_hv_on, "expert"); 
      break;
    case kUnknown: 
      make_switches_disabled(ui->small_hv_off, ui->small_hv_on, "unknown"); 
      break;
  };

  // lecroy switches
  switch(hv_status[k7005]) {
    case kOn:  flick_switch(ui->lecroy_7005_off, ui->lecroy_7005_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->lecroy_7005_on, ui->lecroy_7005_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->lecroy_7005_off, ui->lecroy_7005_on, "x");
    case kUnknown: make_switches_disabled(ui->lecroy_7005_off, ui->lecroy_7005_on, "?");
  };
  switch(hv_status[k7006]) {
    case kOn:  flick_switch(ui->lecroy_7006_off, ui->lecroy_7006_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->lecroy_7006_on, ui->lecroy_7006_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->lecroy_7006_off, ui->lecroy_7006_on, "x");
    case kUnknown: make_switches_disabled(ui->lecroy_7006_off, ui->lecroy_7006_on, "?");
  };
  switch(hv_status[k7007]) {
    case kOn:  flick_switch(ui->lecroy_7007_off, ui->lecroy_7007_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->lecroy_7007_on, ui->lecroy_7007_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->lecroy_7007_off, ui->lecroy_7007_on, "x");
    case kUnknown: make_switches_disabled(ui->lecroy_7007_off, ui->lecroy_7007_on, "?");
  };
  switch(hv_status[k7008]) {
    case kOn:  flick_switch(ui->lecroy_7008_off, ui->lecroy_7008_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->lecroy_7008_on, ui->lecroy_7008_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->lecroy_7008_off, ui->lecroy_7008_on, "x");
    case kUnknown: make_switches_disabled(ui->lecroy_7008_off, ui->lecroy_7008_on, "?");
  };

  // fpd-switch lecroy switches 
  switch(fpd_status[k7005]) {
    case kOn:  flick_switch(ui->fpd_7005_off, ui->fpd_7005_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_7005_on, ui->fpd_7005_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_7005_off, ui->fpd_7005_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_7005_off, ui->fpd_7005_on, "?");
  };
  switch(fpd_status[k7006]) {
    case kOn:  flick_switch(ui->fpd_7006_off, ui->fpd_7006_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_7006_on, ui->fpd_7006_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_7006_off, ui->fpd_7006_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_7006_off, ui->fpd_7006_on, "?");
  };
  switch(fpd_status[k7007]) {
    case kOn:  flick_switch(ui->fpd_7007_off, ui->fpd_7007_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_7007_on, ui->fpd_7007_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_7007_off, ui->fpd_7007_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_7007_off, ui->fpd_7007_on, "?");
  };
  switch(fpd_status[k7008]) {
    case kOn:  flick_switch(ui->fpd_7008_off, ui->fpd_7008_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_7008_on, ui->fpd_7008_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_7008_off, ui->fpd_7008_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_7008_off, ui->fpd_7008_on, "?");
  };

  // fpd-switch small hv and lv
  switch(hv_status[k30V]) {
    case kOn:  flick_switch(ui->fpd_30V_off, ui->fpd_30V_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_30V_on, ui->fpd_30V_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_30V_off, ui->fpd_30V_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_30V_off, ui->fpd_30V_on, "?");
  };
  switch(hv_status[k9V]) {
    case kOn:  flick_switch(ui->fpd_9V_off, ui->fpd_9V_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_9V_on, ui->fpd_9V_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_9V_off, ui->fpd_9V_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_9V_off, ui->fpd_9V_on, "?");
  };
  switch(hv_status[k6V]) {
    case kOn:  flick_switch(ui->fpd_6V_off, ui->fpd_6V_on, 255, 50, 50, kExpert);   break;
    case kOff: flick_switch(ui->fpd_6V_on, ui->fpd_6V_off, 130, 130, 130, kExpert); break;
    case kExpert: make_switches_disabled(ui->fpd_6V_off, ui->fpd_6V_on, "x");
    case kUnknown: make_switches_disabled(ui->fpd_6V_off, ui->fpd_6V_on, "?");
  };

  // disable lecroy FPD switches permanently so no one touches them through GUI; I'm afraid turning on/off
  // the lecroy crates via the FPD switches will result in trouble with the lecroy telnets re-connecting;
  // if we ever need to restart the lecroy crates via the FPD switch, we will do it manually and subsequently
  // recover telnet control manually
  make_switches_disabled(ui->fpd_7005_on,ui->fpd_7005_off,"-");
  make_switches_disabled(ui->fpd_7006_on,ui->fpd_7006_off,"-");
  make_switches_disabled(ui->fpd_7007_on,ui->fpd_7007_off,"-");
  make_switches_disabled(ui->fpd_7008_on,ui->fpd_7008_off,"-");

  // update lights
  ui->light_7005->setPixmap(*hv_light_image[k7005][hv_status[k7005]]);
  ui->light_7006->setPixmap(*hv_light_image[k7006][hv_status[k7006]]);
  ui->light_7007->setPixmap(*hv_light_image[k7007][hv_status[k7007]]);
  ui->light_7008->setPixmap(*hv_light_image[k7008][hv_status[k7008]]);
  ui->light_30V->setPixmap(*hv_light_image[k30V][hv_status[k30V]]);
  ui->light_9V->setPixmap(*hv_light_image[k9V][hv_status[k9V]]);
  ui->light_6V->setPixmap(*hv_light_image[k6V][hv_status[k6V]]);


  // disable small on/off switches if we shift crew is changing large cells
  if(currently_changing_hv[kLarge]) 
    make_switches_disabled(ui->small_hv_on,ui->small_hv_off,"wait");
  // vice versa
  if(currently_changing_hv[kSmall]) 
    make_switches_disabled(ui->large_hv_on,ui->large_hv_off,"wait");
};

void MainWindow::update_uv_panel() {
  switch(check_uv_for_switches(kNorth)) {
    case kOn:  flick_switch(ui->north_uv_off, ui->north_uv_on,  0,   170, 170, kOn);    break;
    case kOff: flick_switch(ui->north_uv_on,  ui->north_uv_off, 130, 130, 130, kOff); break;
    case kExpert: 
      make_switches_disabled(ui->north_uv_off, ui->north_uv_on, "expert");
      break;
    case kUnknown: 
      make_switches_disabled(ui->north_uv_off, ui->north_uv_on, "unknown");
      break;
  };
  switch(check_uv_for_switches(kSouth)) {
    case kOn:  flick_switch(ui->south_uv_off, ui->south_uv_on,  0,   170, 170, kOn);    break;
    case kOff: flick_switch(ui->south_uv_on,  ui->south_uv_off, 130, 130, 130, kOff); break;
    case kExpert: 
      make_switches_disabled(ui->south_uv_off, ui->south_uv_on, "expert");
      break;
    case kUnknown: 
      make_switches_disabled(ui->south_uv_off, ui->south_uv_on, "unknown");
      break;
  };

  // if we're still in a ping loop, disable the involved switches
  if(pinging[kNorth]) make_switches_disabled(ui->north_uv_on, ui->north_uv_off,"ramping");
  if(pinging[kSouth]) make_switches_disabled(ui->south_uv_on, ui->south_uv_off,"ramping");
  if(pinging[kNorth] || pinging[kSouth]) {
    make_switch_changing(ui->update_uv,"* ramping *");
  } else {
    ui->update_uv->setStyleSheet("background-color: rgb(255,85,255); color: black");
    ui->update_uv->setText("Update UV Status");
    ui->update_uv->setDisabled(false);
  };

  // reset bank current bar styles
  ui->bank1_north_status_bar->setStyleSheet("background-color: rgb(50,50,50);");
  ui->bank2_north_status_bar->setStyleSheet("background-color: rgb(50,50,50);");
  ui->bank3_north_status_bar->setStyleSheet("background-color: rgb(50,50,50);");
  ui->bank1_south_status_bar->setStyleSheet("background-color: rgb(50,50,50);");
  ui->bank2_south_status_bar->setStyleSheet("background-color: rgb(50,50,50);");
  ui->bank3_south_status_bar->setStyleSheet("background-color: rgb(50,50,50);");


  // update bank currents and power
  QString current_string;
  QString power_string;
  float current_float;
  float power_float;
  int current_int;

  for(int ns=0; ns<2; ns++) {

    power_float = pdu_power[ns][kWatts];
    power_string = QString("%1 W").arg(QString::number(power_float,'f',1));
    switch(ns) {
      case kNorth: ui->north_power->setText(power_string); break;
      case kSouth: ui->south_power->setText(power_string); break;
    };

    for(int tb=0; tb<2; tb++) {
      for(int ps=0; ps<kNps; ps++) {
        current_float = bank_current[ns][bank_map[ns][tb][ps]];
        current_int = current_float * 10; // (for progress bars)
        if(current_int > 200) current_int = 200;
        if(current_int < 0) current_int = 0;
        current_string = QString("%1 A").arg(QString::number(current_float,'f',1));
        switch(bank_map[ns][tb][ps]) {
          case kB1: 
            switch(ns) {
              case kNorth:
                ui->bank1_north_status_bar->setValue(current_int); 
                ui->bank1_north_status->setText(current_string);
                set_bar_style_sheet(ui->bank1_north_status_bar,current_float);
                break;
              case kSouth:
                ui->bank1_south_status_bar->setValue(current_int); 
                ui->bank1_south_status->setText(current_string);
                set_bar_style_sheet(ui->bank1_south_status_bar,current_float);
                break;
            };
            break;
          case kB2: 
            switch(ns) {
              case kNorth:
                ui->bank2_north_status_bar->setValue(current_int); 
                ui->bank2_north_status->setText(current_string);
                set_bar_style_sheet(ui->bank2_north_status_bar,current_float);
                break;
              case kSouth:
                ui->bank2_south_status_bar->setValue(current_int); 
                ui->bank2_south_status->setText(current_string);
                set_bar_style_sheet(ui->bank2_south_status_bar,current_float);
                break;
            };
            break;
          case kB3: 
            switch(ns) {
              case kNorth:
                ui->bank3_north_status_bar->setValue(current_int); 
                ui->bank3_north_status->setText(current_string);
                set_bar_style_sheet(ui->bank3_north_status_bar,current_float);
                break;
              case kSouth:
                ui->bank3_south_status_bar->setValue(current_int); 
                ui->bank3_south_status->setText(current_string);
                set_bar_style_sheet(ui->bank3_south_status_bar,current_float);
                break;
            };
            break;
        };
      };
    };
  };

  // update lights
  ui->light_n1t->setPixmap(*uv_light_image[uv_status[kNorth][kTop][kps1]]);
  ui->light_n2t->setPixmap(*uv_light_image[uv_status[kNorth][kTop][kps2]]);
  ui->light_n3t->setPixmap(*uv_light_image[uv_status[kNorth][kTop][kps3]]);
  ui->light_n4t->setPixmap(*uv_light_image[uv_status[kNorth][kTop][kps4]]);
  ui->light_n5t->setPixmap(*uv_light_image[uv_status[kNorth][kTop][kps5]]);
  ui->light_n6t->setPixmap(*uv_light_image[uv_status[kNorth][kTop][kps6]]);

  ui->light_n1b->setPixmap(*uv_light_image[uv_status[kNorth][kBottom][kps1]]);
  ui->light_n2b->setPixmap(*uv_light_image[uv_status[kNorth][kBottom][kps2]]);
  ui->light_n3b->setPixmap(*uv_light_image[uv_status[kNorth][kBottom][kps3]]);
  ui->light_n4b->setPixmap(*uv_light_image[uv_status[kNorth][kBottom][kps4]]);
  ui->light_n5b->setPixmap(*uv_light_image[uv_status[kNorth][kBottom][kps5]]);
  ui->light_n6b->setPixmap(*uv_light_image[uv_status[kNorth][kBottom][kps6]]);

  ui->light_s1t->setPixmap(*uv_light_image[uv_status[kSouth][kTop][kps1]]);
  ui->light_s2t->setPixmap(*uv_light_image[uv_status[kSouth][kTop][kps2]]);
  ui->light_s3t->setPixmap(*uv_light_image[uv_status[kSouth][kTop][kps3]]);
  ui->light_s4t->setPixmap(*uv_light_image[uv_status[kSouth][kTop][kps4]]);
  ui->light_s5t->setPixmap(*uv_light_image[uv_status[kSouth][kTop][kps5]]);
  ui->light_s6t->setPixmap(*uv_light_image[uv_status[kSouth][kTop][kps6]]);

  ui->light_s1b->setPixmap(*uv_light_image[uv_status[kSouth][kBottom][kps1]]);
  ui->light_s2b->setPixmap(*uv_light_image[uv_status[kSouth][kBottom][kps2]]);
  ui->light_s3b->setPixmap(*uv_light_image[uv_status[kSouth][kBottom][kps3]]);
  ui->light_s4b->setPixmap(*uv_light_image[uv_status[kSouth][kBottom][kps4]]);
  ui->light_s5b->setPixmap(*uv_light_image[uv_status[kSouth][kBottom][kps5]]);
  ui->light_s6b->setPixmap(*uv_light_image[uv_status[kSouth][kBottom][kps6]]);

  if(update_uv_clicked) {
    message = QString("uvLEDs bank currents:");
    print_log(kNote,message);
    message = QString("----- nB1=%1A | nB2=%2A | nB3=%3A").arg(
      QString::number(bank_current[kNorth][kB1],'f',1),
      QString::number(bank_current[kNorth][kB2],'f',1),
      QString::number(bank_current[kNorth][kB3],'f',1)
    );
    print_log(kNote,message);
    message = QString("----- sB1=%1A | sB2=%2A | sB3=%3A").arg(
      QString::number(bank_current[kSouth][kB1],'f',1),
      QString::number(bank_current[kSouth][kB2],'f',1),
      QString::number(bank_current[kSouth][kB3],'f',1)
    );
    print_log(kNote,message);
  };
};


void MainWindow::update_interlock_panel() {
  // check hv and uv overall states
  int hv_state = check_overall_hv_state();
  int uv_state = check_overall_uv_state();

  message = QString("hv_state=%1  uv_state=%2").arg(QString::number(hv_state),QString::number(uv_state));
  print_log(kDebug,message);

  int hv_interlock_status_old = hv_interlock_status;
  int uv_interlock_status_old = uv_interlock_status;


  // if not in panic mode, set the interlock statuses based on
  // the overall states
  if(!major_problem) {
    if(hv_state==kOff && uv_state==kOff) {
      // if both are off, unlock UV only if we asked for HV status update and
      //                  unlock HV only if we asked for UV status update
      if(update_uv_clicked && uv_update_only==uAll) hv_interlock_status = kUnlocked;
      if(update_hv_clicked && hv_update_only==uAll) uv_interlock_status = kUnlocked;
    }
    else if(hv_state==kOn && uv_state==kOff) {
      // if HV is on, lock UV control
      hv_interlock_status = kUnlocked;
      uv_interlock_status = kLocked;
    }
    else if(hv_state==kOff && uv_state==kOn) {
      // if UV is on, lock HV control
      hv_interlock_status = kLocked;
      uv_interlock_status = kUnlocked;
    }
    else if(hv_state==kUnknown || uv_state==kUnknown) {
      // lock hv if uv state is unknown
      if(uv_state==kUnknown) hv_interlock_status = kLocked;
      if(hv_state==kUnknown) uv_interlock_status = kLocked;
    }
    else {
      // if both UV and HV are on, engage panic mode and turn off both 
      // HV and UV
      major_problem = true;
      ui->interlock_status_message->setText("<font color=\"Red\">-- PANIC MODE LOCKOUT --</font>");
      message = QString("MAJOR PROBLEM! MAJOR PROBLEM!\nHV and UV simultaneously on!");
      print_status(kCritical,message);
      for(int xx=0; xx<15; xx++) print_status(kError,message);
      delay(1);
      print_log(kAction,"SENDING AUTOMATIC PANIC OFF...");

      setPanic_off();

    };
  };


  // panic mode total lock-out
  if(major_problem) {
    hv_interlock_status = kLocked;
    uv_interlock_status = kLocked;

    ui->interlock_status_message->setText("<font color=\"Red\">-- PANIC MODE LOCKOUT --</font>");
    return;
  };


  // start timers (only needed if last known state of UV and HV were both off)
  if(hv_state==kOff && uv_state==kOff) {
    hv_update_timer->start();
    uv_update_timer->start();
  } else {
    hv_update_timer->stop();
    uv_update_timer->stop();
  };


  // set HV interlock status
  update_hv_panel(); // update lights and switches
  if(hv_interlock_status == kUnlocked) {
    ui->hv_interlock_icon->setPixmap(*lock_icon[kUnlocked]);
  } else {
    lock_hv();
    message = QString("HV controls %1").arg(interlock_status_name[hv_interlock_status]);
    ui->interlock_status_message->setText(message);
  };

  // set UV interlock status
  update_uv_panel(); // update lights and switches
  if(uv_interlock_status == kUnlocked) {
    ui->uv_interlock_icon->setPixmap(*lock_icon[kUnlocked]);
  } else {
    lock_uv();
    message = QString("UV controls %1").arg(interlock_status_name[uv_interlock_status]);
    ui->interlock_status_message->setText(message);
  };

  // update interlock status message
  if(hv_interlock_status != hv_interlock_status_old) {
    message = QString("HV controls %1").arg(interlock_status_name[hv_interlock_status]);
    ui->interlock_status_message->setText(message);
  }
  if(uv_interlock_status != uv_interlock_status_old) {
    message = QString("UV controls %1").arg(interlock_status_name[uv_interlock_status]);
    ui->interlock_status_message->setText(message);
  }
  if(hv_interlock_status != hv_interlock_status_old &&
     uv_interlock_status != uv_interlock_status_old) {
    message = QString("<p>HV controls %1</p><p>UV controls %2").arg(
      interlock_status_name[hv_interlock_status],
      interlock_status_name[uv_interlock_status]);
    ui->interlock_status_message->setText(message);
  }

  // disable lecroy FPD switches permanently so no one touches them through GUI; I'm afraid turning on/off
  // the lecroy crates via the FPD switches will result in trouble with the lecroy telnets re-connecting;
  // if we ever need to restart the lecroy crates via the FPD switch, we will do it manually and subsequently
  // recover telnet control manually
  make_switches_disabled(ui->fpd_7005_on,ui->fpd_7005_off,"-");
  make_switches_disabled(ui->fpd_7006_on,ui->fpd_7006_off,"-");
  make_switches_disabled(ui->fpd_7007_on,ui->fpd_7007_off,"-");
  make_switches_disabled(ui->fpd_7008_on,ui->fpd_7008_off,"-");
};


void MainWindow::flick_switch(QPushButton * b_col, 
                              QPushButton * b_blk,
                              int rr, int gg, int bb,
                              int state) {
  char sty[128];
  sprintf(sty,"background-color: rgb(%d,%d,%d); color: black",rr,gg,bb);
  b_blk->setStyleSheet(sty_black);
  b_col->setStyleSheet(sty);
  b_blk->setDisabled(true);
  b_col->setDisabled(false);
  b_blk->setText("-");
  switch(state) {
    case kOn:  b_col->setText("Turn Off");  break;
    case kOff: b_col->setText("Turn On"); break;
    default: b_col->setText(""); 
  };
};



void MainWindow::make_switches_disabled(QPushButton * sw1, QPushButton * sw2, const char * txt) {
  sw1->setStyleSheet("background-color: black; color: red");
  sw2->setStyleSheet("background-color: black; color: red");
  sw1->setDisabled(true);
  sw2->setDisabled(true);
  sw1->setText(txt);
  sw2->setText(txt);
};

void MainWindow::make_switch_changing(QPushButton * sw, const char * txt) {
  sw->setStyleSheet("background-color: rgb(0,255,255); color: black");
  sw->setDisabled(true);
  sw->setDisabled(true);
  sw->setText(txt);
};


void MainWindow::check_uv_critical() {
  delay(1); // obligatory
  bool is_critical = false;
  bool which_critical[2][3];
  for(int nn=0; nn<2; nn++) {
    for(int bb=0; bb<3; bb++) {
      which_critical[nn][bb] = false;
      if(bank_current[nn][bb]>alarm_current) {
        is_critical = true;
        which_critical[nn][bb] = true;
      };
    };
  };
  if(is_critical) {
    message = QString("uvLED PDU bank current load\nabove %1 A threshold").arg(alarm_current);
    print_status(kCritical,message);
    for(int nn=0; nn<2; nn++) {
      for(int bb=0; bb<3; bb++) {
        if(which_critical[nn][bb]) {
          message = QString("[-----] %1 PDU B%2 at %3 A").arg(
            pdu_name[nn],
            QString::number(bb+1),
            QString::number(bank_current[nn][bb],'f',1)
          );
          print_log(kCritical,message);
        };
      };
    };
    changeUV(kNorth,kOff,true);
    changeUV(kSouth,kOff,true);
    message = QString("NOW CLICK UPDATE UV STATUS\nTO VERIFY UVLEDS ARE OFF\n(and call expert)");
    print_status(kNote,message);
  };
};



void MainWindow::print_status(int status_type, QString message_) {
  QString formatted_message = message_.replace(QString("\n"),QString("</p><p>"));
  switch(status_type) {
    case kNote:
      ui->status_message_hv->setStyleSheet("color: rgb(50,255,50)");
      formatted_message = QString("<html><p>%1</p></html>").arg(message_);
      break;
    case kAction:
      ui->status_message_hv->setStyleSheet("color: rgb(255,255,50)");
      formatted_message = QString("<html><p>%1</p></html>").arg(message_);
      break;
    case kUpdateMessage:
      ui->status_message_hv->setStyleSheet("color: rgb(50,255,50)");
      formatted_message = QString("<html><p>%1</p></html>").arg(message_);
      break;
    case kError:
      ui->status_message_hv->setStyleSheet("color: rgb(255,50,50)");
      formatted_message = QString("<html><p>ERROR</p><p>%1</p></html>").arg(message_);
      break;
    case kCritical:
      ui->status_message_hv->setStyleSheet("color: rgb(255,0,255)");
      formatted_message = QString("<html><p>C R I T I C A L</p><p>%1</p></html>").arg(message_);
      break;
    case kBackend:
      ui->status_message_hv->setStyleSheet("color: #D2B48C");
      formatted_message = QString("<html><p>%1</p></html>").arg(message_);
      break;
    case kDebug:
      ui->status_message_hv->setStyleSheet("color: #D2B48C");
      formatted_message = QString("<html><p>%1</p></html>").arg(message_);
      break;
  };
  ui->status_message_hv->setText(formatted_message);

  print_log(status_type,message_);
};

void MainWindow::print_log(int status_type, QString message_) {
  if(status_type==kDebug && debug==false) return;

  timestamp = QDateTime::currentDateTime();

  // prepare text for printing log to gui
  QString log_message = message_;
  log_message = log_message.replace(QString("</p><p>"),QString(" "));
  log_message = log_message.replace(QString("\n"),QString(" "));
  QString log_message_formatted = 
    QString("<font color=\"Lime\">[%1]</font> %2: %3</font><br />").arg(
      timestamp.toString("ddd, MMM d @ hh:mm:ss"),
      message_type_str[status_type],
      log_message
    );
  log_scroll = ui->log_window->verticalScrollBar();
  log_scroll->setValue(log_scroll->maximum());
  ui->log_window->moveCursor(QTextCursor::End);

  // print 
  if(status_type != kSMSonly) {
    // print to window
    ui->log_window->insertHtml(log_message_formatted);

    // print log to output file
    logstream.open(logfile_name,std::ios::app);
    logstream << log_message_formatted.toStdString().data() << std::endl; 
    logstream.close();
  };


  // send text message for critical status logs
  if(status_type == kCritical || status_type == kSMSonly) { 
    sprintf(exe_textmessage,"%s; ./%s \"%s\"; %s",
      pushdlog,
      "send_text_message.sh",
      log_message.toStdString().data(),
      popd
    );
    system(exe_textmessage);
  };
};

void MainWindow::set_bar_style_sheet(QProgressBar * bar, float curr) {
  QString color;
  float threshold[4] = {10,
                        17.0,
                        17.5,
                        alarm_current
  };
  if(curr<threshold[0])                            color = QString("04dfd8");
  else if(curr>=threshold[0] && curr<threshold[1]) color = QString("00ff48");
  else if(curr>=threshold[1] && curr<threshold[2]) color = QString("fbff03");
  else if(curr>=threshold[2] && curr<threshold[3]) color = QString("ffa508");
  else                                             color = QString("ff0202");

  QString bar_style = QString(
    "QProgressBar {"
      "background-color: black;"
      "border-radius: 5px;"
      "padding: 3px;"
    "}"
    "QProgressBar::chunk {"
      "background-color: #%1;"
      "border-radius: 5px;"
    "}"
  ).arg(color);
  bar->setStyleSheet(bar_style);
};

void MainWindow::lock_hv() {
  ui->hv_interlock_icon->setPixmap(*lock_icon[kLocked]);
  make_switches_disabled(ui->large_hv_on,ui->large_hv_off,"locked");
  make_switches_disabled(ui->small_hv_on,ui->small_hv_off,"locked");
  if(hv_status[k7005] != kOn) make_switches_disabled(ui->lecroy_7005_on,ui->lecroy_7005_off,"x");
  if(hv_status[k7006] != kOn) make_switches_disabled(ui->lecroy_7006_on,ui->lecroy_7006_off,"x");
  if(hv_status[k7007] != kOn) make_switches_disabled(ui->lecroy_7007_on,ui->lecroy_7007_off,"x");
  if(hv_status[k7008] != kOn) make_switches_disabled(ui->lecroy_7008_on,ui->lecroy_7008_off,"x");
  //make_switches_disabled(ui->fpd_7005_on,ui->fpd_7005_off,"x");
  //make_switches_disabled(ui->fpd_7006_on,ui->fpd_7006_off,"x");
  //make_switches_disabled(ui->fpd_7007_on,ui->fpd_7007_off,"x");
  //make_switches_disabled(ui->fpd_7008_on,ui->fpd_7008_off,"x");
  if(hv_status[k6V] != kOn) make_switches_disabled(ui->fpd_6V_on,ui->fpd_6V_off,"x");
  if(hv_status[k9V] != kOn) make_switches_disabled(ui->fpd_9V_on,ui->fpd_9V_off,"x");
  if(hv_status[k30V] != kOn) make_switches_disabled(ui->fpd_30V_on,ui->fpd_30V_off,"x");
  ui->interlock_status_message->setText("HV controls LOCKED"); // overriden in update_interlock_panel
  hv_update_timer->stop();
  hv_interlock_status = kLocked; // need to lock again here if this was called by timeout() signal
};

void MainWindow::lock_uv() {
  ui->uv_interlock_icon->setPixmap(*lock_icon[kLocked]);
  make_switches_disabled(ui->north_uv_on,ui->north_uv_off,"locked");
  make_switches_disabled(ui->south_uv_on,ui->south_uv_off,"locked");
  ui->interlock_status_message->setText("UV controls LOCKED"); // overriden in update_interlock_panel
  uv_update_timer->stop();
  uv_interlock_status = kLocked; // need to lock again here if this was called by timeout() signal
};


// execute a backend script, called "scriptname", located at "scriptdir", and set outname to "scriptdir/output/outname"
// if do_it == true,  the output names and script names are set and the script is subsequently executed
// if do_it == false, the output names and script names are set and the script is NEVER executed; this case is
//    used when you *only* want to configure the filenames
void MainWindow::execute_script(bool do_it) {
  sprintf(exe,"%s; ./%s; %s",pushd,scriptname,popd);

  if(debug_backend) {
    print_log(kBackend,QString("scriptdir -=- %1").arg(scriptdir));
    print_log(kBackend,QString("scriptname -=- %1").arg(scriptname));
    print_log(kBackend,QString("pushd -=- %1").arg(pushd));
    print_log(kBackend,QString("popd -=- %1").arg(popd));
    print_log(kBackend,QString("exe -=- %1").arg(exe));
  };

  sprintf(outname_tmp,"%s/output/%s",scriptdir,outname);
  sprintf(outname,"%s",outname_tmp);

  if(debug_backend) {
    print_log(kBackend,QString("outname=%1").arg(outname));
  };

  if(do_it) system(exe); // call the executable
};

void MainWindow::initialize_statuses() {
  if(!simulation_mode) {
    // MAKE EVERYTHING UNKNOWN --------------------------- 
    ///*
    for(int k=0; k<kNhvs; k++) { hv_status[k]=kUnknown; fpd_status[k]=kUnknown; };
    hv_status[k6V]=kUnknown;
    hv_status[k9V]=kUnknown;
    fpd_status[k30V]=kUnknown;
    for(int h=0; h<2; h++) {
      for(int n=0; n<2; n++) for(int p=0; p<kNps; p++) uv_status[h][n][p]=kUnknown;
      for(int b=0; b<3; b++) bank_current[h][b] = -1.0;
      for(int u=0; u<2; u++) pdu_power[h][u] = -1.0;
    };
    //*/
    // TEST HV SECTOR---------------------------
    /*
    for(int k=0; k<kNhvs; k++) { hv_status[k]=kUnknown; fpd_status[k]=kOn; };
    hv_status[k6V]=kUnknown;
    hv_status[k9V]=kUnknown;
    fpd_status[k30V]=kUnknown;
    for(int h=0; h<2; h++) {
      for(int n=0; n<2; n++) for(int p=0; p<kNps; p++) uv_status[h][n][p]=kOff;
      for(int b=0; b<3; b++) bank_current[h][b] = 0;
      for(int u=0; u<2; u++) pdu_power[h][u] = 0;
    };
    */
    // TEST UV SECTOR---------------------------
    /*
    for(int k=0; k<kNhvs; k++) { hv_status[k]=kOff; fpd_status[k]=kOff; };
    hv_status[k6V]=kOff;
    hv_status[k9V]=kOff;
    fpd_status[k30V]=kOff;
    for(int h=0; h<2; h++) {
      for(int n=0; n<2; n++) for(int p=0; p<kNps; p++) uv_status[h][n][p]=kUnknown;
      for(int b=0; b<3; b++) bank_current[h][b] = -1.0;
      for(int u=0; u<2; u++) pdu_power[h][u] = -1.0;
    };
    */
  } else {
    // initially set hv off
    ///*
    for(int k=0; k<kNhvs; k++) { hv_status[k]=kOff; fpd_status[k]=kOff; };
    hv_status[k6V]=kOn; fpd_status[k6V]=kOn;
    hv_status[k9V]=kOn; fpd_status[k9V]=kOn;
    fpd_status[k7005]=kOn;
    fpd_status[k7006]=kOn;
    fpd_status[k7007]=kOn;
    fpd_status[k7008]=kOn;
    // initially set uv off
    for(int h=0; h<2; h++) {
      for(int n=0; n<2; n++) for(int p=0; p<kNps; p++) uv_status[h][n][p]=kOff; 
      for(int b=0; b<3; b++) bank_current[h][b] = 0;
    };
    //*/
    /*
    // testing panic mode (with hv and uv on)
    for(int k=0; k<kNhvs; k++) { hv_status[k]=kOn; fpd_status[k]=kOn; };
    hv_status[k6V]=kOn;
    hv_status[k9V]=kOn;
    fpd_status[k30V]=kOn;
    // initially set uv off
    for(int h=0; h<2; h++) {
      for(int n=0; n<2; n++) for(int p=0; p<kNps; p++) uv_status[h][n][p]=kOn; 
      for(int b=0; b<3; b++) bank_current[h][b] = 16;
    };
    */
  };
};



void MainWindow::censor_expert_ctrl() {
  message = QString("-- expert controls have been hidden");
  print_log(kNote,message);
  ui->expert_control_censor->setGeometry(1080,39,321,441);
  censor_timer->stop();
};

void MainWindow::uncensor_expert_ctrl() {
  message = QString("-- expert controls have been opened");
  print_log(kNote,message);
  ui->expert_control_censor->setGeometry(1380,39,21,441);
  censor_timer->start();
};


// miscellaneous :::::::::::::::::::::::::::::::::::::::::::::::
void MainWindow::delay(int seconds) {
  QTime dieTime= QTime::currentTime().addMSecs(seconds*1000);
  while (QTime::currentTime() < dieTime)
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
};



// simulation methods :::::::::::::::::::::::::::::::::::::::::::::::
void MainWindow::simulate_changeHV(int which_hv, int setting) {
  delay(1); 

  bool allowed_in_sim = false; // (for small cells hv only)

  if(which_hv==k7005 || which_hv==k7006 || 
      which_hv==k7007 || which_hv==k7008) {

    if(fpd_status[which_hv] == kOn) {
      hv_status[which_hv] = setting;
      message = QString("Large cells %1\nHV changed to %2").arg(hv_name[which_hv],state_name[setting]);
      print_status(kAction,message);
    } else {
      message = QString("cannot change %1\nwith FPD switch off or unknown").arg(hv_name[which_hv]);
      print_status(kError,message);
      error_occured = true;
    };
  } 

  else if(which_hv==k30V || which_hv==k9V || which_hv==k6V) {

    switch(which_hv) {
      case k30V:
        if(hv_status[k9V]==kOn && hv_status[k6V]==kOn &&
            fpd_status[k9V]==kOn && fpd_status[k6V]==kOn) allowed_in_sim = true;
        break;
      case k9V:
        if(hv_status[k30V]==kOff && hv_status[k6V]==kOn &&
            fpd_status[k30V]==kOff && fpd_status[k6V]==kOn) allowed_in_sim = true;
        break;
      case k6V:
        if(hv_status[k30V]==kOff && hv_status[k9V]==kOff &&
            fpd_status[k30V]==kOff && fpd_status[k9V]==kOff) allowed_in_sim = true;
        break;
    };
    if(allowed_in_sim) {       
      hv_status[which_hv] = setting;
      fpd_status[which_hv] = setting;
      message = QString("Small Cells %1\nchanged to %2").arg(hv_name[which_hv],state_name[setting]);
      print_status(kAction,message);
    } else {
      message = QString("Small Cell HV/LV cannot\nbe switched on/off out of order");
      print_status(kError,message);
      error_occured = true;
    };
  }
};


void MainWindow::simulate_changeLecroyFpd(int which_hv, int setting) {
  delay(1); 
  if(which_hv==k7005 || which_hv==k7006 || 
      which_hv==k7007 || which_hv==k7008) {

    if(hv_status[which_hv] == kOff) 
    {
      message = QString("%1 power from FPD\nswitch changed to %2").arg(hv_name[which_hv],state_name[setting]);
      print_status(kAction,message);
      fpd_status[which_hv] = setting;
    }
    else {
      message = QString("cannot change FPD Lecroy\nswitch with crate on");
      print_status(kError,message);
    };
  };
};


void MainWindow::simulate_changeUV(int which_pdu, int setting, bool panic) {
  if(panic) {
    message = QString("Pressed UV PANIC OFF on %1 PDU!").arg(pdu_name[which_pdu]);
    print_status(kAction,message);
    message = QString("PANIC ACTION: turning %1 uvLEDs\n%2 PDU...").arg(state_name[setting],pdu_name[which_pdu]);
    print_status(kCritical,message);

    delay(1);
    for(int tt=0; tt<2; tt++) {
      for(int ps=kps1; ps<kNps; ps++) {
        uv_status[which_pdu][tt][ps] = setting;
        bank_current[which_pdu][bank_map[which_pdu][tt][ps]] = 0; 
      };
    };
  } else {

    message = QString("turning %1 uvLEDs\n%2 PDU...").arg(state_name[setting],pdu_name[which_pdu]);
    print_status(kAction,message);

    // testing simulation
    float interval, factor, dividend;
    switch(which_pdu) {
      case kNorth:
        interval = setting==kOn ? 2.0:-2.0;
        factor = setting==kOn ? 1:-1;
        dividend = 5.0;
        break;
      case kSouth:
        interval = setting==kOn ? 2.5:-2.5;
        factor = setting==kOn ? 1:-1;
        dividend = 1.0;
        break;
    };
    delay(1);
    for(int ps=kps1; ps<=kps6; ps++) {
      uv_status[which_pdu][kTop][ps] = setting;
      bank_current[which_pdu][bank_map[which_pdu][kTop][ps]] += 
        interval + factor*((float)bank_map[which_pdu][kTop][ps]/dividend) +
        0;
      //factor*(which_pdu==kNorth && bank_map[which_pdu][kTop][ps]==kB1)*3.5 -
      //factor*(which_pdu==kNorth && bank_map[which_pdu][kTop][ps]==kB3)*4.0;
    };
    update_interlock_panel();
    delay(1);
    for(int ps=kps6; ps>=kps1; ps--) {
      uv_status[which_pdu][kBottom][ps] = setting;
      bank_current[which_pdu][bank_map[which_pdu][kBottom][ps]] +=
        interval + factor*((float)bank_map[which_pdu][kBottom][ps]/dividend);
    };
    update_interlock_panel();

  };
};
