#!/bin/bash


# FORMAT [item] [status]
# if item<100, it's an outlet status:
#   item = outlet number
#   status = 0 -- off
#            1 -- on
#            2 -- changing
#            9 -- unknown
# if 100<item<200, it's a bank current
#   item = bank # + 100
#   status = current * 10 (so we can read it in as int!)
#          = -10 if there was a problem
# if 200<item, it's a power reading
#   item = 201 -- VA
#        = 202 -- Watts
#   status = number * 10
#          = -10 if there was a problem

## this script just sends the "status/currents/power" part
## to uvled_status.parse.sh to take care of

pdu=$1

out_file="output/uvled_${pdu}_status.out"

> $out_file

read_file="output/uvled_${pdu}_change.out"

cnt=$(grep -c "status" $read_file)
if [ $cnt -lt 1 ]; then 
  echo "ERROR" > $out_file
else
  grep -A100 "status" $read_file > $out_file
fi

./uvled_status.parse.sh ${pdu}
