#!/bin/bash

# checks if a certain output file claims access failure, 
# due to another process using the telnet connection

str=$(grep -E 'Connected to fpdswitch.trg.bnl.local' $1)
if [ -z "$str" ]; then
  echo "ERROR: TELNET CONFLICT ENCOUNTERED"
fi
