#!/bin/bash
# formats the lecroy state statuses into a data table for front-end

# FORMAT: [crate] [status]
# status = 0 -- crate off
#        = 1 -- crate on
#        = 4 -- crate ramping
#        = 8 -- crate status indeterminate (lecroy physically off?)
#        = 9 -- conflicting telnet session

out_file="output/lecroy_status.tab"
> $out_file

# status string to look for
grep_str="The high voltage is currently "


# loop through the payload output files
echo ""
echo "crate statuses:"
for crate in {7005..7008}; do 
  stat_file="output/status${crate}.out"

  # check for telnet conflict
  err_str=$(./check_telnet_lecroy_conflict.sh ${stat_file})

  # determine the status
  ramp_str=$(grep "${grep_str}" ${stat_file} | grep "ramping")
  if [ -n "$ramp_str" ]; then
    stat_str="ramping"
  else
    stat_str=$(grep "${grep_str}" ${stat_file} | sed "s/${grep_str}//g")
    stat_str=$(echo $stat_str | sed 's/\..*$//')
  fi
  echo "${crate} -- ${err_str}${stat_str}" 

  # if there's no error, output to data table
  if [ -n "$err_str" ]; then statnum=9;
  else
    if [ "${stat_str}" == "on" ]; then statnum=1 
    elif [ "${stat_str}" == "ON" ]; then statnum=1 
    elif [ "${stat_str}" == "off" ]; then statnum=0
    elif [ "${stat_str}" == "OFF" ]; then statnum=0
    elif [ "${stat_str}" == "ramping" ]; then statnum=4
    else statnum=8
    fi
  fi
  echo "$crate $statnum" >> $out_file
done

# print the data table
echo ""
echo "output file: ${out_file}"
cat $out_file
