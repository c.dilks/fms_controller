#!/bin/bash

pdu=$1
change=$2

echo "[+] sending ${change} request payload to ${pdu} PDU..."
./uvled_change.payload.sh ${pdu} ${change}

echo "[+] done; now parsing..."
./uvled_change.parse.sh ${pdu}
