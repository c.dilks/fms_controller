#
# check uvled status
#
pdu=$1
change=$2
setting="$(head -n1 /home/fms_hv/.configuracion)"
outlet=$3
#
(
echo -e "device";
sleep 1;
echo -e "${setting} -c\r"
sleep 3;
echo -e "\rdelayed${change} ${outlet}\r";
sleep 2;
echo -e "\rstatus\r";
sleep 2;
echo -e "\rcurrent\r";
sleep 2;
echo -e "\rpower\r";
sleep 2;
echo -e "\rbye\r";
sleep 1;
echo -e "";
sleep 1;
echo -e "quit\n\r";
sleep 1;
) | telnet fms-${pdu}-uv.starp.bnl.gov > output/uvled_${pdu}_change.out
