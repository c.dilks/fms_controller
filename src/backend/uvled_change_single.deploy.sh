#!/bin/bash

pdu=$1
change=$2
outlet=$3

echo "[+] sending ${change} request payload to ${pdu} PDU..."
./uvled_change_single.payload.sh ${pdu} ${change} ${outlet}

echo "[+] done; now parsing..."
./uvled_change.parse.sh ${pdu}
