#!/bin/bash
#
# deploys payload to change lecroy crate status
#

crate=$1
setting=$2

# send payload 
ssh trg 'bash -s' < \
  lecroy_crate_change.payload.sh ${crate} ${setting} fmsfiles/status${crate}.out &
echo "[+] sent payload for lecroy ${crate}"

# wait for payload to complete
sleep 1
echo "[+] payload sent; waiting for status check to finish..."
wait

# copy the result to local dir
scp trg:/home/startrg/staruser/fmsfiles/status700*.out output
echo ""
echo "[+] output copied to local dir."

# parse the output
echo "[+] parsing output..."
echo ""
./lecroy_crate_status.parse.sh
