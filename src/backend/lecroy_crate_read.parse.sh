#!/bin/bash
#
# FORMAT [crate number] [average voltage (=99999 if error)]
#
#

out_file="output/lecroy_read.tab"
> $out_file

# loop through output files for each crate
for crate in {7005..7008}; do 
  read_file="output/read${crate}.out"

  # telnet conflict check
  err_str=$(./check_telnet_lecroy_conflict.sh ${read_file})
  read_str=""
  
  # if no error, pull out the voltage-read-back column
  if [ -z "$err_str" ]; then
    #grep -E '(..,..)' $read_file | grep -v read | sed 's/(.*)//' | awk '{print $4}' > ${read_file}.dat
    grep -E '(..,..)' $read_file | grep -v read | sed 's/(.*)//' | sed 's/-//g;s/+//g' | awk '{print $2}' > ${read_file}.dat

    # compute the average voltage read-back
    ave=$(./compute_average.pl ${read_file}.dat)
    read_str="average V_read = ${ave}"
    outnum=${ave}
  else
    outnum=99999
  fi
  echo "${crate} -- ${err_str}${read_str}"
  echo "${crate} ${outnum}" >> ${out_file}
done

# print output file
echo ""
echo "output file: ${out_file}"
cat $out_file
