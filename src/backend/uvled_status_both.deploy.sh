#!/bin/bash

echo "[+] sending status request payload to PDUs"
./uvled_status.payload.sh north &
./uvled_status.payload.sh south &

sleep 1
echo "[+] waiting for response..."
wait


echo "[+] done; now parsing..."
./uvled_status.parse.sh north
./uvled_status.parse.sh south
