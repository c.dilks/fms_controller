#!/bin/bash
# just prints a uvled status output file with only error codes

out_file=$1

for o in {1..24}; do echo "$o 9" >> $out_file; done
for b in {101..103}; do echo "$b -10" >> $out_file; done
for p in {201..202}; do echo "$p -10" >> $out_file; done

cat $out_file;
