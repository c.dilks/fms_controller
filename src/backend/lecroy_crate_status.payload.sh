#
# check status single lecroy crate using "show status"
#
cratenum=$1
filename=$2
#
(
echo -e "\r";
sleep 1;
echo -e "show status\r";
sleep 3;
echo -e "";
sleep 1;
echo -e "quit\n\r";
sleep 1;
) | telnet fms-serv.trg.bnl.local ${cratenum} > ${filename}
