#!/bin/bash

plugname="$1"
setting="$2"

# determine plug name
if [ "$plugname" == "7005" ]; then plug=1
elif [ "$plugname" == "7006" ]; then plug=2
elif [ "$plugname" == "7007" ]; then plug=3
elif [ "$plugname" == "7008" ]; then plug=4
elif [ "$plugname" == "30V" ]; then plug=5
elif [ "$plugname" == "9V" ]; then plug=6
elif [ "$plugname" == "6V" ]; then plug=7
else 
  echo "ERROR: invalid plug name"
  exit
fi

# send payload
echo "[+] sending payload for FPD switch change plug ${plug} (${plugname}) to ${setting}"
ssh trg 'bash -s' < \
  fpd_switch_change.payload.sh ${plug} ${setting} fmsfiles/changeFPD.out

# wait for response
sleep 1
wait

# copy output file to local dir
scp trg:/home/startrg/staruser/fmsfiles/changeFPD.out output/
echo ""
echo "[+] switch changed."
echo "[+] output copied to local dir."

# parse output
./fpd_switch_change.parse.sh
