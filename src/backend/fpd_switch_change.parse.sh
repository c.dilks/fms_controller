#!/bin/bash

# takes updated plug status table from output of fpd_switch_change.payload.sh
# and writes it to the file that fpd_switch_status.parse.sh reads; then
# subsequently executes fpd_switch_status.parse.sh

read_file="output/changeFPD.out"
out_file="output/statusFPD.out"

# send "connected" output to out_file (needed for error checking)
grep -E 'Connected to fpdswitch.trg.bnl.local' ${read_file} > ${out_file}
grep -A50 "Processing \- please wait" ${read_file} >> ${out_file}

# call status parser
./fpd_switch_status.parse.sh
