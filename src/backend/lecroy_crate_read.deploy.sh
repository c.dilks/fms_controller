#!/bin/bash
#
# deploys payload to check voltage read-backs of lecroy crates
#


# send payload to each crate, running them in the background
for crate in {7005..7008}; do 
  ssh trg 'bash -s' < \
    lecroy_crate_read.payload.sh ${crate} fmsfiles/read${crate}.out &
  echo "[+] sent payload for lecroy ${crate}"
done

# wait for all payloads to complete
sleep 1
echo "[+] payloads sent; waiting for readbacks to finish..."
wait

# copy the result to local dir
scp trg:/home/startrg/staruser/fmsfiles/read700*.out output
echo ""
echo "[+] output copied to local dir."

# parse the output
echo "[+] parsing output..."
echo ""
./lecroy_crate_read.parse.sh
