#!/bin/bash


# FORMAT [item] [status]
# if item<100, it's an outlet status:
#   item = outlet number
#   status = 0 -- off
#            1 -- on
#            2 -- changing
#            9 -- unknown
# if 100<item<200, it's a bank current
#   item = bank # + 100
#   status = current * 10 (so we can read it in as int!)
#          = -10 if there was a problem
# if 200<item, it's a power reading
#   item = 201 -- VA
#        = 202 -- Watts
#   status = number * 10
#          = -10 if there was a problem


pdu=$1

out_file="output/uvled_${pdu}_status.tab"

> $out_file

read_file="output/uvled_${pdu}_status.out"

# check if we got in by assuming a minimum number of lines
lines=$(cat ${read_file} | wc -l)
if [ $lines -lt 10 ]; then
  echo ""
  echo ""
  echo "ERROR ${read_file} has too few lines..  connection probably failed!"
  echo ""
  echo ""
  for o in {1..24}; do echo "$o 9" >> $out_file; done
  for b in {101..103}; do echo "$b -10" >> $out_file; done
  for p in {201..202}; do echo "$p -10" >> $out_file; done
  exit
fi


# get outlet statuses
echo ""
echo "outlet statuses:"
outdat="output/uvled_${pdu}_outlets.dat"

grep -E ":.*:" ${read_file} | sed 's/://g' > ${outdat}
lines=$(cat ${outdat} | wc -l)

if [ $lines -ne 24 ]; then
  echo "ERROR: problem with reading outlet statuses"
  for o in {1..24}; do echo "$o 9" >> $out_file; done
else
  while read line; do
    outlet=$(echo $line | awk '{print $1}')
    statstr=$(echo $line | awk '{print $2}')
    if [ "$statstr" == "OFF" ]; then statnum=0
    elif [ "$statstr" == "ON" ]; then statnum=1
    elif [ -n "$(echo ${statstr} | grep "*")" ]; then statnum=2
    else statnum=9
    fi
    echo "${outlet} -- ${statstr}"
    echo "${outlet} ${statnum}" >> $out_file
  done < ${outdat}
fi

  
# get current loads
echo ""
echo "bank currents:"
outdat="output/uvled_${pdu}_currents.dat"

grep -A5 "current" ${read_file} | grep Bank | sed 's/://g;s/A//g' > ${outdat}
lines=$(cat ${outdat} | wc -l)

if [ $lines -ne 3 ]; then
  echo "ERROR: problem reading currents"
  for b in {101..103}; do echo "$b -10" >> $out_file; done
else
  while read line; do
    bank=$(echo $line | awk '{print $2}')
    current=$(echo $line | awk '{print $3}')
    banknum=$(perl -le "print ${bank}+100")
    currentnum=$(perl -le "print ${current}*10")
    echo "$bank -- $current"
    echo "${banknum} ${currentnum}" >> $out_file
  done < ${outdat}
fi


# get power usage
echo ""
echo "power:"
outdat="output/uvled_${pdu}_powers.dat"

grep -A5 "power" ${read_file} | grep -E "VA|Watts" > ${outdat}
lines=$(cat ${outdat} | wc -l)

if [ $lines -ne 2 ]; then
  echo "ERROR: problem reading powers"
  for p in {201..202}; do echo "$p -10" >> $out_file; done
else
  power_va=$(grep "VA" ${outdat} | awk '{print $1}')
  power_watts=$(grep "Watts" ${outdat} | awk '{print $1}')
  power_va_num=$(perl -le "print ${power_va}*10")
  power_watts_num=$(perl -le "print ${power_watts}*10")
  echo "${power_va} (V-A)"
  echo "${power_watts} (Watts)"
  echo "201 ${power_va_num}" >> $out_file
  echo "202 ${power_watts_num}" >> $out_file
fi
