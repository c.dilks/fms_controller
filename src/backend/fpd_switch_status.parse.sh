#!/bin/bash
#
# FORMAT: [crate number OR small cell V] [status]
# status = 0 -- switch off
#        = 1 -- switch on
#        = 8 -- unknown problem
#        = 9 -- telnet conflict

out_file="output/fpd_status.tab"
> $out_file

read_file="output/statusFPD.out"

# check for telnet conflict
err_str=$(./check_telnet_fpd_conflict.sh ${read_file})
if [ -n "${err_str}" ]; then
  echo "TELNET CONFLICT! unable to check status!"
  echo "7005 9" >> ${out_file}
  echo "7006 9" >> ${out_file}
  echo "7007 9" >> ${out_file}
  echo "7008 9" >> ${out_file}
  echo "30 9" >> ${out_file}
  echo "9 9" >> ${out_file}
  echo "6 9" >> ${out_file}
  cat ${out_file}
  exit
fi

# obtain table lines
line7005=$(grep -E "1.*North_7005" ${read_file})
line7006=$(grep -E "2.*North_7006" ${read_file})
line7007=$(grep -E "3.*South_7007" ${read_file})
line7008=$(grep -E "4.*South_7008" ${read_file})
line30V=$(grep -E "5.*POS30V" ${read_file})
line9V=$(grep -E "6.*POS9V" ${read_file})
line6V=$(grep -E "7.*NEG6V" ${read_file})


# print table lines
echo $line7005
echo $line7006
echo $line7007
echo $line7008
echo $line30V
echo $line9V
echo $line6V


# obtain statuses
stat7005=$(echo $line7005 | awk '{print $5}')
stat7006=$(echo $line7006 | awk '{print $5}')
stat7007=$(echo $line7007 | awk '{print $5}')
stat7008=$(echo $line7008 | awk '{print $5}')
stat30V=$(echo $line30V | awk '{print $5}')
stat9V=$(echo $line9V | awk '{print $5}')
stat6V=$(echo $line6V | awk '{print $5}')


# format statuses to status numbers
if [ "${stat7005}" == "OFF" ]; then num7005=0
elif [ "${stat7005}" == "ON" ]; then num7005=1
else num7005=8
fi

if [ "${stat7006}" == "OFF" ]; then num7006=0
elif [ "${stat7006}" == "ON" ]; then num7006=1
else num7006=8
fi

if [ "${stat7007}" == "OFF" ]; then num7007=0
elif [ "${stat7007}" == "ON" ]; then num7007=1
else num7007=8
fi

if [ "${stat7008}" == "OFF" ]; then num7008=0
elif [ "${stat7008}" == "ON" ]; then num7008=1
else num7008=8
fi

if [ "${stat30V}" == "OFF" ]; then num30V=0
elif [ "${stat30V}" == "ON" ]; then num30V=1
else num30V=8
fi

if [ "${stat9V}" == "OFF" ]; then num9V=0
elif [ "${stat9V}" == "ON" ]; then num9V=1
else num9V=8
fi

if [ "${stat6V}" == "OFF" ]; then num6V=0
elif [ "${stat6V}" == "ON" ]; then num6V=1
else num6V=8
fi

# output status numbers to output file
echo "7005 ${num7005}" >> ${out_file}
echo "7006 ${num7006}" >> ${out_file}
echo "7007 ${num7007}" >> ${out_file}
echo "7008 ${num7008}" >> ${out_file}
echo "30 ${num30V}" >> ${out_file}
echo "9 ${num9V}" >> ${out_file}
echo "6 ${num6V}" >> ${out_file}


# print output file
echo ""
echo "output file: ${out_file}"
cat $out_file
