#!/bin/bash

# checks if a certain output file claims access failure, 
# due to another process using the telnet connection

str=$(grep -E 'tty.*is being used by' $1)
if [ -n "$str" ]; then
  echo "ERROR: TELNET CONFLICT ENCOUNTERED"
fi
