#!/bin/bash
#
# deploys payload to check lecroy status, using command "show status"
#

# send payload for each crate, running them in the background
for crate in {7005..7008}; do 
  ssh trg 'bash -s' < \
    lecroy_crate_status.payload.sh ${crate} fmsfiles/status${crate}.out &
  echo "[+] sent payload for lecroy ${crate} status check"
done

# wait for all payloads to complete
sleep 1
echo "[+] payloads sent; waiting for readbacks to finish..."
wait

# copy the result to local dir
scp trg:/home/startrg/staruser/fmsfiles/status700*.out output/
echo ""
echo "[+] output copied to local dir."

# parse the result by calling the parser
echo "[+] parsing output..."
echo ""
./lecroy_crate_status.parse.sh
