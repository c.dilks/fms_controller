#
# reads a single lecroy crate
#
cratenum=$1
filename=$2
#
(
echo -e "\r";
sleep 1;
echo -e "read (0-15,0-15)\r";
sleep 10;
echo -e "";
sleep 1;
echo -e "quit\n\r";
sleep 1;
) | telnet fms-serv.trg.bnl.local ${cratenum} > ${filename}
