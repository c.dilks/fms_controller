#!/bin/bash
#
# FORMAT: [crate] [status] 
# status = 0 -- crate off
#        = 1 -- crate on
#        = 8 -- crate status indeterminate (lecroy physically off?)
#        = 9 -- conflicting telnet session
#
#

crate=$1


out_file="output/lecroy_status.tab"
> $out_file
read_file="output/change${crate}.out"

grep_str="The high voltage is currently "

# telnet conflict check
err_str=$(./check_telnet_lecroy_conflict.sh ${read_file})
ave_str=""


# if no error, pull out the voltage-read-back column and check status
if [ -z "$err_str" ]; then
  
  # check status
  stat_str=$(grep "${grep_str}" ${read_file} | sed "s/${grep_str}//g")
  stat_str=$(echo $stat_str | sed 's/\..*$//')
  echo "${crate} -- ${err_str}${stat_str}" 
  if [ "${stat_str}" == "on" ]; then statnum=1 
  elif [ "${stat_str}" == "ON" ]; then statnum=1 
  elif [ "${stat_str}" == "off" ]; then statnum=0
  elif [ "${stat_str}" == "OFF" ]; then statnum=0
  else statnum=8
  fi

  # extract voltage read-backs (DEPRECATED use here)
  # grep -E '(..,..)' $read_file | grep -v read | sed 's/(.*)//' | awk '{print $4}' > ${read_file}.dat

  # compute the average voltage read-back (DEPRECATED use here)
  #ave=$(./compute_average.pl ${read_file}.dat)
  #ave_str="average V_read = ${ave}"
  #ave_out=${ave}
else
  statnum=9
  #ave_out=99999
fi

#echo "${crate} -- ${stat_str} -- ${err_str}${ave_str}" # DEPRECATED USE
#echo "${crate} ${statnum} ${ave_out}" >> ${out_file}   # DEPRECATED USE
echo "${crate} ${statnum}" >> ${out_file}

# print output file
echo ""
echo "output file: ${out_file}"
cat $out_file
