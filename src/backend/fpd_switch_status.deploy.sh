#!/bin/bash

# send payload
echo "[+] sending payload for FPD switch status"
ssh trg 'bash -s' < \
  fpd_switch_status.payload.sh fmsfiles/statusFPD.out

# wait for response
sleep 1
wait

# copy output to local dir
scp trg:/home/startrg/staruser/fmsfiles/statusFPD.out output/
echo ""
echo "[+] output copied to local dir."

# parse output
echo "[+] parsing output..."
echo ""
./fpd_switch_status.parse.sh
