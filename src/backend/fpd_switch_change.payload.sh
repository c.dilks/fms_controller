#
# changes FPD switch status
#
# for some reason, no \r needed after first line when trying
# to turn on/off a switch
#
plug=$1
setting=$2
filename=$3
#
(
echo -e "/${setting} ${plug}";
sleep 1;
echo -e "y\r";
sleep 1;
echo -e "/x\r";
sleep 1;
echo -e "y\r";
sleep 1;
) | telnet fpdswitch.trg.bnl.local > ${filename}
