#!/bin/bash

pdu=$1

echo "[+] sending status request payload to ${pdu} PDU..."
./uvled_status.payload.sh ${pdu}

echo "[+] done; now parsing..."
./uvled_status.parse.sh ${pdu}
