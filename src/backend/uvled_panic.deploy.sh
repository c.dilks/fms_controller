#!/bin/bash

echo "[+] sending panic off request payload to both PDUs..."
./uvled_panic.payload.sh north &
./uvled_panic.payload.sh south &

echo "[+] payloads sent; please wait..."
wait

echo "[+] done; now parsing..."
./uvled_change.parse.sh north
./uvled_change.parse.sh south
