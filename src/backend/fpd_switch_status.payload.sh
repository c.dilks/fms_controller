#
# reads FPD switch status
#
filename=$1
#
(
echo -e "/x";
sleep 1;
echo -e "y\r";
sleep 1;
) | telnet fpdswitch.trg.bnl.local > ${filename}
