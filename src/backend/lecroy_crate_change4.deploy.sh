#!/bin/bash
#
# deploys payload to change status of all 4 lecroy crates at once
#

setting=$1

# send payload 
for crate in {7005..7008}; do 
  ssh trg 'bash -s' < \
    lecroy_crate_change.payload.sh ${crate} ${setting} fmsfiles/status${crate}.out &
  echo "[+] sent payload for lecroy ${crate}"
done

# wait for payload to complete
sleep 1
echo "[+] payloads sent; waiting for status check to finish..."
wait

# copy the result to local dir
scp trg:/home/startrg/staruser/fmsfiles/status700*.out output
echo ""
echo "[+] output copied to local dir."

# parse the output
echo "[+] parsing output..."
echo ""
./lecroy_crate_status.parse.sh
