#
# reads a single lecroy crate
#
cratenum=$1
setting=$2
filename=$3
#
(
echo -e "\r";
sleep 1;
echo -e "${setting}\r";
sleep 3;
echo -e "show status\r";
sleep 2;
echo -e "";
sleep 1;
echo -e "quit\n\r";
sleep 1;
) | telnet fms-serv.trg.bnl.local ${cratenum} > ${filename}
