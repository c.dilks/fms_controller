#!/bin/bash
logfile="LOGFILE.html"
touch $logfile
grepstr=$(grep -E "html.*body.*bgcolor" $logfile)
if [ -z "$grepstr" ]; then
  echo '<html><body bgcolor="#1D1D1D">' > tempo
  cat $logfile >> tempo
  mv tempo $logfile
fi
