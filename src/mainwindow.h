#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include <QTimer>
#include <QPushButton>
#include <QString>
#include <QScrollBar>
#include <QDateTime>
#include <QTextCursor>
#include <QProgressBar>

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <strings.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void setLarge_on(); void setLarge_off();
    void setSmall_on(); void setSmall_off();
    void setUpdate_hv_status();

    void set7005_on(); void set7006_on(); void set7007_on(); void set7008_on();
    void set7005_off(); void set7006_off(); void set7007_off(); void set7008_off();

    void set7005fpd_on(); void set7006fpd_on(); void set7007fpd_on(); void set7008fpd_on();
    void set30V_on(); void set9V_on(); void set6V_on();

    void set7005fpd_off(); void set7006fpd_off(); void set7007fpd_off(); void set7008fpd_off();
    void set30V_off(); void set9V_off(); void set6V_off();

    void setNorthUV_on(); void setNorthUV_off();
    void setSouthUV_on(); void setSouthUV_off();
    void setPanic_off();
    void setUpdate_uv_status();

    void lock_hv();
    void lock_uv();

    void ping_north();
    void ping_south();

    void censor_expert_ctrl();
    void uncensor_expert_ctrl();


private:
    enum lights_enum {k7005,k7006,k7007,k7008,k6V,k9V,k30V,kNhvs};
    enum status_enum {kOff,kOn,kUpdating,kUnknown,kRamping,kNstatuses,kExpert,kNotImportant};
    enum cell_enum {kLarge,kSmall};
    enum message_enum {kNote,kAction,kError,kCritical,kBackend,kDebug,kUpdateMessage,kSMSonly,kNmessageTypes};
    enum uv_ns {kNorth,kSouth};
    enum uv_tb {kTop,kBottom};
    enum uv_ps {kps1,kps2,kps3,kps4,kps5,kps6,kNps};
    enum bank_enum {kB1, kB2, kB3};
    enum interlock_enum {kLocked,kUnlocked};
    enum update_only_enum {uAll,uLecroy,uFPD,uNorth,uSouth};
    enum power_units_enum {kVA,kWatts};



    void changeHV(int which_hv, int setting);
    void changeLecroyFpd(int which_hv, int setting);
    void changeUV(int which_pdu, int setting, bool panic);

    int check_large_hv_for_switches();
    int check_small_hv_for_switches();
    int check_uv_for_switches(int northsouth);

    int check_overall_hv_state();
    int check_overall_uv_state();

    void change_large_lights(int setting);
    void change_small_lights(int setting);
    void change_uv_lights(int setting);

    void update_hv_panel();
    void update_uv_panel();
    void update_interlock_panel();
    void flick_switch(QPushButton * b_i,
                      QPushButton * b_f,
                      int rr, int gg, int bb,
                      int state);
    void make_switches_disabled(QPushButton * sw1, QPushButton * sw2, const char * txt);
    void make_switch_changing(QPushButton * sw, const char * txt);
    void check_uv_critical();
    void print_status(int status_type, QString message_);
    void print_log(int status_type, QString message_);
    void set_bar_style_sheet(QProgressBar * bar, float curr);
    
    void execute_script(bool do_it);

    void delay(int seconds);

    void simulate_changeHV(int which_hv, int setting);
    void simulate_changeLecroyFpd(int which_hv, int setting);
    void simulate_changeUV(int which_pdu, int setting, bool panic); // 737 & 767 (panic)

    void ping_pdu(int which_pdu);

    void initialize_statuses();






    Ui::MainWindow *ui;
    QPixmap * hv_light_image[kNhvs][kNstatuses];
    QPixmap * uv_light_image[kNstatuses];
    QPixmap * lock_icon[2]; // [locked / unlocked]

    int new_hv_status;
    int new_uv_status;
    int hv_status[kNhvs];
    int fpd_status[kNhvs];
    int uv_status[2][2][kNps]; // [north/south] [top/bottom] [power supply #]
    int hv_interlock_status;
    int uv_interlock_status;
    float bank_current[2][3]; // [north/south] [bank 1/2/3]
    float pdu_power[2][2]; // [north/south] [V-A / W]
    int bank_map[2][2][kNps]; // maps: uv power supply --> bank number-1
    char sty_black[128];
    char hv_name[kNhvs][32];
    char hv_name_short[kNhvs][32];
    char pdu_name[2][8];
    char pdu_name_short[2][8];
    char state_name[kNstatuses][16]; 
    char state_name_short[kNstatuses][16]; // only on or off
    char bank_name[3][8];
    char interlock_status_name[2][16];
    bool currently_changing_hv[2]; // [large/small]
    bool currently_changing_uv[2]; // [north/south]
    QString message;
    QString message_type_str[kNmessageTypes];
    float alarm_current;
    bool major_problem;

    bool update_uv_clicked;
    bool update_hv_clicked;
    bool change_four;

    int envia_hv_mensaje[2]; // [on/off]
    int envia_uv_mensaje[2]; // [on/off]

    bool simulation_mode;
    int debug_backend;
    bool debug;
    int lecroy_ramp_time;

    int hv_update_only;
    int uv_update_only;
    bool error_occured;

    char scriptdir[512];
    char logdir[512];
    char pushd[1024];
    char pushdlog[1024];
    char popd[128];
    char scriptname[512];
    char exe[2048];
    char exe_textmessage[2048];
    char exe_beginlog[2048];
    char outname[2048];
    char outname_tmp[2048];
    int buffer[10]; // for reading text files
    float ave_volt;
    float voltage_threshold;
    int this_crate;
    int this_bank;
    int these_power_units;

    
    QScrollBar * log_scroll;
    QDateTime timestamp;
    char logfile_name[1024];

    QTimer * hv_update_timer;
    QTimer * uv_update_timer;

    QTimer * uv_change_timer[2]; // [north/south]
    int ping_cnt[2];
    bool pinging[2];

    QTimer * censor_timer;

    std::ofstream logstream;


};

#endif // MAINWINDOW_H
