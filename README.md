FMS controller
==============
Software interlock to control FMS HV and UV for run 17


Building and Running
--------------------

NOTE: to run as a "simulation" (i.e., without activating back-end
control scripts), make sure `simulation_mode = true` in 
`src/mainwindow.cpp` before building

1. Easiest build method is via `qtcreator`; open `src/FMS_controller.pro` 
  * you may need to install relevant `qt` libraries 
2. Building with `qtcreator` executes the following:
  * create a local `build` directory
  * run `qmake` to generate a `Makefile`
  * run `make` to generate a binary: `<your_build_dir>/FMS_controller`
3. Execute the binary (can also be done within `qtcreator`)

